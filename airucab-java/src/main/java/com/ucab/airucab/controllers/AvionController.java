	package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import com.ucab.airucab.models.*;
import com.ucab.airucab.views.*;

public class AvionController implements ActionListener {
	public AvionCreateView vistaCrear;
	public Avion avion;
	public Validaciones validar;
	
	public AvionController() {
		this.avion = new Avion();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	
	
private void initializerShowView( ) {
		
		vistaCrear = new AvionCreateView();
		vistaCrear.table_pieza.setModel(avion.tableShow(vistaCrear.col));
		vistaCrear.table_pieza.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                //tableMouseClicked(e);
            }
        });
this.vistaCrear.btnCrear.addActionListener(this);
//this.show.btn_agregar.addActionListener(this);
this.vistaCrear.FrInicio.setVisible(true);
}

public boolean validarAvion(AvionCreateView vistaCrear){
boolean flag=true ;
if(!validar.validarSoloLetras( vistaCrear.txtNombre.getText())) {
	vistaCrear.txtNombre.setText("debe ingresar solo letras");
	flag = false;			
}
else {
	avion.setNombre(vistaCrear.txtNombre.getText());
	System.out.println("nombre"+avion.getNombre());
}
if (!validar.validarVacio(vistaCrear.txtModelo.getText())){
	vistaCrear.txtModelo.setText("Este campo no puede quedar vacio");
	flag =false;
}
else{
	avion.setModelo(vistaCrear.txtModelo.getText());
	System.out.println("descripcion"+avion.getModelo());
}

if (!validar.validarSoloLetras(vistaCrear.txtTipo.getText())){
	vistaCrear.txtTipo.setText("Este campo no puede quedar vacio");
	flag =false;
}
else{
	avion.setTipo((vistaCrear.txtTipo.getText()));
	System.out.println("Tipo nave"+avion.getTipo() );
}
if (!validar.validarSoloNumeros(vistaCrear.txtLongitud.getText())){
	System.out.println("Id"+vistaCrear.txtLongitud.getText());
	vistaCrear.txtLongitud.setText("Este campo solo acepta numeros");
	flag =false;
}
else{
	avion.setLongitud((Integer.parseInt(vistaCrear.txtLongitud.getText())));
	System.out.println("Longitud"+avion.getLongitud());
}
if (!validar.validarSoloNumeros(vistaCrear.txtCapacidad.getText())){
	System.out.println("Capacidad"+vistaCrear.txtCapacidad.getText());
	vistaCrear.txtCapacidad.setText("Este campo solo acepta numeros");
	flag =false;
}
else{
	avion.setCapacidad((Integer.parseInt(vistaCrear.txtCapacidad.getText())));
	System.out.println("Capacidad"+avion.getCapacidad());
}
if (!validar.validarSoloNumeros(vistaCrear.txtAltura.getText())){
	System.out.println("Altura"+vistaCrear.txtAltura.getText());
	vistaCrear.txtAltura.setText("Este campo solo acepta numeros");
	flag =false;
}
else{
	avion.setAltura((Integer.parseInt(vistaCrear.txtAltura.getText())));
	System.out.println("Altura"+avion.getAltura());
}
return flag;

}

private void agregarPieza() {
for (int i = 0; i < vistaCrear.table_pieza.getRowCount(); i++) {
	Boolean chked = Boolean.valueOf(vistaCrear.table_pieza.getValueAt(i, 4)
			.toString());
	String dataCol1 = vistaCrear.table_pieza.getValueAt(i, 0).toString();
	if (chked) {
		Pieza pieza = new Pieza();
		pieza.setId(Integer.parseInt(vistaCrear.table_pieza.getValueAt(i, 0).toString())); ;
		avion.getPiezas().add(pieza);
		JOptionPane.showMessageDialog(null, dataCol1);
		
	}  
}
}


@Override
public void actionPerformed(ActionEvent e) {

if (vistaCrear.btnCrear == e.getSource()) {
	if ( this.validarAvion(vistaCrear)) {
		agregarPieza();

	if(!avion.getPiezas().isEmpty() && avion.crearAvion()) {
		
		System.out.println("hola");
		
	}else
		System.out.println("adios");
    }
    }
}
}