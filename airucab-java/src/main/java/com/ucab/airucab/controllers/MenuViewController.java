package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.ucab.airucab.models.Reportes;
import com.ucab.airucab.views.MenuView;
import com.ucab.airucab.views.ReportesView;

public class MenuViewController implements ActionListener{
	
	public MenuView vista;
	public Reportes reportes;
	public ReportesView show;
	
	public MenuViewController(){
		this.initializerView();
		
	}
	
	public void initializerView(){
		vista = new MenuView();
		
		this.vista.btnAlasUtilizadas.addActionListener(this);
		//this.vista.btnCantidadDeProductos.addActionListener(this);
		this.vista.btnCantMediaAviones.addActionListener(this);
		this.vista.btnFallospiezas.addActionListener(this);
		this.vista.btnFallosMaterial.addActionListener(this);
		this.vista.btnEspecificacionesDeModelo.addActionListener(this);
		this.vista.btnDescripcionPiezas.addActionListener(this);
		this.vista.btnInventarioMensual.addActionListener(this);
		this.vista.btnListadoDeProveedores.addActionListener(this);
		this.vista.btnMejorCliente.addActionListener(this);
		this.vista.btnModeloAvion.addActionListener(this);
		this.vista.btnModeloVendido.addActionListener(this);
		this.vista.btnProdAnual.addActionListener(this);
		this.vista.btnProdProduccion.addActionListener(this);
		this.vista.btnProductoMas.addActionListener(this);
		this.vista.btnProductoMasPedidoMaterial.addActionListener(this);
		this.vista.btnProductoMasPedidoAvion.addActionListener(this);

		this.vista.btnProductoMasPedidoPieza.addActionListener(this);
		this.vista.btnInventarioMensualPiezas.addActionListener(this);
		vista.FrInicio.setVisible(true);
	} 
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(vista.btnProdAnual == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ReadProduccionAnual());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnProdProduccion == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ReadPromedioProduccionMensual());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnMejorCliente == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.TopTenClientes());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnModeloAvion == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ModeloAvion());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnCantMediaAviones == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.CantidadMediadeAvionesMes());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnModeloVendido == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ModeloMasVendido());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnAlasUtilizadas == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.TipoAlaUtilizada());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnFallosMaterial == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.FallosMaterial());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		if (vista.btnFallospiezas== e.getSource()) {
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.FallosPiezas());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnListadoDeProveedores == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ListaProvedores());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnInventarioMensual == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.InventarioMateriales());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		if(vista.btnInventarioMensualPiezas == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.InventarioPiezas());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		
		if(vista.btnEspecificacionesDeModelo == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.EspecificacionesModelos());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		
		if(vista.btnDescripcionPiezas == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.descripcionPiezas());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
		if(vista.btnProductoMasPedidoMaterial == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ProductoMasPedidoMaterial());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		if(vista.btnProductoMasPedidoAvion == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ProductoMasPedidoAvion());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		if(vista.btnProductoMasPedidoPieza == e.getSource()){
			show = new ReportesView();
			reportes = new Reportes();
			show.table.setModel(
					reportes.ProductoMasPedidoPieza());
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                //tableMouseClicked(e);
	            }
	        });
			show.FrInicio.setVisible(true);
		}
		
	}

}