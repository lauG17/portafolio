package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

import com.ucab.airucab.models.*;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.*;





public class PedidoController implements ActionListener {
	public PedidoCreateView vistaCrear;
	public Pedido pedido;
	public Validaciones validar;
	
	public PedidoController (){
	
		this.pedido = new Pedido();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	
	private void initializerView( ) {
		this.vistaCrear = new PedidoCreateView();		
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		vistaCrear = new PedidoCreateView();
		vistaCrear.tableCliente.setModel(pedido.tableShowCliente(vistaCrear.col));
		this.vistaCrear.btn_agregar.addActionListener(this);
		vistaCrear.tableCliente.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                //tableMouseClicked(e);
            }
        });
	    vistaCrear.tableAvion.setModel(pedido.tableShowAviones(vistaCrear.col));
	    this.vistaCrear.FrInicio.setVisible(true);
	}
	

	
	public boolean validarPedido(PedidoCreateView vistaCrear){
		boolean flag=true ;
		
		if(!validar.validarSoloNumeros(vistaCrear.text_pago.getText())) {
			vistaCrear.text_pago.setText("debe ingresar solo letras");
			flag = false;			
		}
		else {//recordar arreglar decimales
			pedido.getFactura().setPago(Integer.parseInt(vistaCrear.text_pago.getText()));
		}
				
		
		return flag;
		
	}
	
	private void agregarCliente() {
		for (int i = 0; i < vistaCrear.tableCliente.getRowCount(); i++) {
			Boolean chked = Boolean.valueOf(vistaCrear.tableCliente.getValueAt(i, 2)
					.toString());
			String dataCol1 = vistaCrear.tableCliente.getValueAt(i, 0).toString();
			if (chked) {
                pedido.setCliente(Integer.parseInt(dataCol1));
				JOptionPane.showMessageDialog(null, dataCol1);
				
			}  
		}
	}
	
	private void agregarAvion() {
		for (int i = 0; i < vistaCrear.tableAvion.getRowCount(); i++) {
			Boolean chked = Boolean.valueOf(vistaCrear.tableAvion.getValueAt(i, 3)
					.toString());
			String dataCol1 = vistaCrear.tableAvion.getValueAt(i, 0).toString();
			if (chked) {
				pedido.getAvion().setId(Integer.parseInt(dataCol1));
				JOptionPane.showMessageDialog(null, dataCol1);
				
			}  
		}
	}
	
	

	public boolean PrimerPaso() {
		
		boolean flat = true;
		
		agregarAvion();
		agregarCliente();
		if (vistaCrear.comboBox.getSelectedItem().toString().equals("Efectivo")){
			pedido.getFactura().setTipoPago(1);
		}
		if (vistaCrear.comboBox.getSelectedItem().toString().equals("Cheque")){
			pedido.getFactura().setTipoPago(2);
		}
		if (vistaCrear.comboBox.getSelectedItem().toString().equals("Debito")){
			pedido.getFactura().setTipoPago(3);
		}
		if (vistaCrear.comboBox.getSelectedItem().toString().equals("Credito")){
			pedido.getFactura().setTipoPago(4);
		}
		pedido.getFactura().setFechaString("18/12/12");
		flat = pedido.getFactura().create();
		if (flat) {
			System.out.println("factura paso");
			flat = pedido.create();
		}else {
			System.out.println("factura no paso");
		}
		
        
		return flat;
	}
	
	public boolean SegundoPaso() {
		
		boolean flat = true;
	    flat = pedido.getAvion().llenarPiezas();
	    flat = pedido.getProveedor().pedidoProveedor(pedido.getAvion().getPiezas());
		return true;
	}
	
	public boolean TercerPaso() {
		// TODO pedido pieza
		boolean flat = true;
		flat = pedido.getAvion().getPieza().pedidoProveedor(pedido.getAvion().getPiezas(),pedido.getId());
		return flat;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		

		if ( this.validarPedido(vistaCrear)) {
		
			if(vistaCrear.btn_agregar == e.getSource()){
				if (PrimerPaso()) {
					if(SegundoPaso()) {
						if(TercerPaso()) {
							
						}else {
							System.out.println("no paso tercer");
						}
					}else {
						System.out.println("no paso segundo");
					}
				}
			}else {
				System.out.println("no paso primero");
			}
		}

		
	}
	
     /* private void tableMouseClicked(MouseEvent e) {
        
        int column = vistaCrear.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/vistaCrear.table.getRowHeight();
        Pieza pieza = new Pieza();
        
        if(row < vistaCrear.table.getRowCount() && row >= 0 && column < vistaCrear.table.getColumnCount() && column >= 0){
            Object value = vistaCrear.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(vistaCrear.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Cliente?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");

                }
                if(boton.getName().equals("v")){
                    System.out.println("Click en el boton ver");
                  
                }
            }

        }*/


}
    
    
