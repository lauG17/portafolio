package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.airucab.models.EquipoPrueba;
import com.ucab.airucab.models.Sede;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.EquipoPruebaCreateView;

public class EquipoPruebaController implements ActionListener{

	public EquipoPrueba equipoPrueba;
	public EquipoPruebaCreateView vistaCrear;
	public Validaciones validar;
	
	public EquipoPruebaController(){
		this.equipoPrueba=new EquipoPrueba();
		this.validar = new Validaciones();
		initializerShowView();
	}
	
	private void initializerView( ) {
		this.vistaCrear = new EquipoPruebaCreateView();		
		this.vistaCrear.FrInicio.setVisible(true);

		initializerShowView( );
	} 
	private void initializerShowView( ) {
		
		vistaCrear = new EquipoPruebaCreateView();
		for(Sede sede: equipoPrueba.MostrarSede()){
			vistaCrear.comboBox.addItem(sede.getNombre());
			
		}
		vistaCrear.btnCrearEquipo.addActionListener(this);
		vistaCrear.table.setModel(equipoPrueba.tableShow(vistaCrear.col,vistaCrear.check_material));
		vistaCrear.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		//this.show.btn_agregar.addActionListener(this);
	    this.vistaCrear.FrInicio.setVisible(true);
	}
	
	
	public boolean ValidarEquipoPrueba(EquipoPruebaCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.txtNombre.getText())) {
			vistaCrear.txtNombre.setText("debe ingresar solo letras");
			flag = false;			
		}
		else {
			equipoPrueba.setNombre(vistaCrear.txtNombre.getText());	
		}
		return flag;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(vistaCrear.btnCrearEquipo== e.getSource()){
			if(ValidarEquipoPrueba(vistaCrear)){
				
			}
		}
		
	}
	
	 private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
	        
	        int column = vistaCrear.table.getColumnModel().getColumnIndexAtX(e.getX());
	        int row = e.getY()/vistaCrear.table.getRowHeight();
	        EquipoPrueba equipoprueba = new EquipoPrueba();
	        
	        if(row < vistaCrear.table.getRowCount() && row >= 0 && column < vistaCrear.table.getColumnCount() && column >= 0){
	            Object value = vistaCrear.table.getValueAt(row, column);
	        }
	}

}
