package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;


import com.ucab.airucab.models.Proveedor;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.ProveedorShowView;
import com.ucab.airucab.views.*;


public class ProveedorController implements ActionListener {
	public ProveedorShowView show;
	public ProveedorCreateView vistaCrear;
	public Validaciones validar;
	public Proveedor proveedor;
	
	public ProveedorController (){
		this.proveedor = new Proveedor();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	
	private void initializerView( ) {
		
		this.vistaCrear = new ProveedorCreateView();			
		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.btnAgregarCorreos.addActionListener(this);
		this.vistaCrear.btnAgregarRedes.addActionListener(this);
		this.vistaCrear.btnAgregarTelefonos.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
private void initializerShowView( ) {
		
		show = new ProveedorShowView();
		show.table.setModel(
				proveedor.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	

	
	public boolean validarProveedor ( ProveedorCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.txtNombre.getText())) {
			vistaCrear.txtNombre.setText("debe ingresar solo letras");
			flag = false;
			
		}
		else {
			proveedor.setNombre(vistaCrear.txtNombre.getText());
			System.out.print(proveedor.getNombre());
		}
		if (!validar.validarVacio(vistaCrear.txtRif.getText())){
			vistaCrear.txtRif.setText("Este campo no puede quedar vacio");
			flag =false;
		}
		else{
			proveedor.setRif(vistaCrear.txtRif.getText());
			System.out.println(proveedor.getRif());
		}
		if (vistaCrear.calendar.getDate() == null){
			System.out.println("ingrese una fecha");
			flag = false;
		}else
			proveedor.getFecha().setFechaDate(new Date(vistaCrear.calendar.getDate().getTime()));
				
		
		return flag;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(this.show.btn_agregar ==e.getSource()){
			initializerView( );
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			if(validarProveedor(vistaCrear)){
				
				if (proveedor.crearProveedor()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
            		proveedor.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

				}
				else{
			  		System.out.println("adios");
				}
			}
		}
		
		if(this.vistaCrear.btnRegresar==e.getSource()){
			
			
		}
		if(this.vistaCrear.btnAgregarTelefonos==e.getSource()){
			
		}
		
	}
private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Proveedor proveedor = new Proveedor();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Proveedor?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    proveedor.setRif(show.table.getValueAt(row, 0).toString());
                    proveedor.eliminarProveedor();
                    show.table.setModel(
                    		proveedor.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
                if(boton.getName().equals("v")){
                    System.out.println("Click en el boton ver");
                    //EVENTOS MODIFICAR
                }
            }

        }


}


}
