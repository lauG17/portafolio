package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.airucab.models.Beneficiario;

import com.ucab.airucab.models.Empleado;
import com.ucab.airucab.models.Sede;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.EmpleadoShowView;
import com.ucab.airucab.views.BeneficiarioCreateView;
import com.ucab.airucab.views.EmpleadoCreateView;


public class EmpleadoController  implements ActionListener{

	public EmpleadoShowView show;
	public EmpleadoCreateView vistaCrear;
	public Empleado empleado;
	public Validaciones validar;
	public Beneficiario beneficiario;
	public BeneficiarioCreateView vistaCrearBeneficiario;
	public Validaciones validarBeneficiarios;
	
	public EmpleadoController (){
		this.beneficiario= new Beneficiario();
		this.empleado = new Empleado();
		this.validar = new Validaciones();
		this.validarBeneficiarios= new Validaciones();
		this.initializerShowView();
	}
	
	private void initializerView() {

		this.vistaCrear = new EmpleadoCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.btnAgregarCorreos.addActionListener(this);
		this.vistaCrear.btnAgregarRedes.addActionListener(this);
		this.vistaCrear.btnAgregarTelefonos.addActionListener(this);
		this.vistaCrear.btnAgregarBeneficiarios.addActionListener(this);
		this.vistaCrearBeneficiario = new BeneficiarioCreateView();	
		this.vistaCrear.FrInicio.setVisible(true);
		for(Sede sede: empleado.MostrarSede()){
			vistaCrear.comboBox.addItem(sede.getNombre());
			
		}
	}
	private void initializerShowView( ) {
		
		show = new EmpleadoShowView();
		show.table.setModel(
				empleado.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	
	
	private void initializerBeneficiarioCreateView(){
			//this.vistaCrearBeneficiario = new BeneficiarioCreateView();		
			this.vistaCrearBeneficiario.btnRegistrar.addActionListener(this);
			this.vistaCrearBeneficiario.btnRegresar.addActionListener(this);
			this.vistaCrearBeneficiario.FrInicio.setVisible(true);
	
	}
	
	public boolean ValidarEmpleado(EmpleadoCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.txtNombre.getText())){
			vistaCrear.txtNombre.setText("debe ingresar solo letras");
			flag = false;
		}
		else{
			empleado.setNombre(vistaCrear.txtNombre.getText());
			System.out.print(empleado.getNombre());
		}
		if(!validar.validarSoloLetras(vistaCrear.txtApellido.getText())){
			vistaCrear.txtApellido.setText("debe ingresar solo letras");
			flag = false;
		}
		else{
			empleado.setNombre(vistaCrear.txtApellido.getText());
			System.out.print(empleado.getApellido());
		}
		if(!validar.validarSoloNumeros(vistaCrear.txtCi.getText())){
			vistaCrear.txtCi.setText("Debe ingresar solo numeros");
			flag=false;
		}
		else{
			empleado.setCi(Integer.parseInt(vistaCrear.txtCi.getText()));
			System.out.print(empleado.getCi());
		}
		
		if(!validar.validarSoloLetras(vistaCrear.txtCargo.getText())){
			vistaCrear.txtCargo.setText("debe ingresar solo letras");
			flag = false;
		}
		else{
			empleado.setTitulo(vistaCrear.txtCargo.getText());
			System.out.print(empleado.getCargoempleado());
		}
		if(!validar.validarSoloLetras(vistaCrear.txtTitulo.getText())){
			vistaCrear.txtTitulo.setText("debe ingresar solo letras");
			flag = false;
		}
		else{
			empleado.setTitulo(vistaCrear.txtTitulo.getText());
			System.out.print(empleado.getTitulo());
		}
		if (vistaCrear.calendar.getDate() == null){
			System.out.println("ingrese una fecha");
			flag = false;
		}else{
			empleado.getFecha().setFechaDate(new Date(vistaCrear.calendar.getDate().getTime()));
		
		}
		for(Sede sede: empleado.MostrarSede()){
			if (vistaCrear.comboBox.getSelectedItem().toString().equals(sede.getNombre())){
				empleado.setSede(sede.getCodigo() );
			}
			
		}

		return flag;
	}
	public boolean ValidarBeneficiario(BeneficiarioCreateView vistaCrearBeneficiario){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrearBeneficiario.txtNombre.getText())){
			vistaCrearBeneficiario.txtNombre.setText("debe ingresar solo letras");
			flag = false;
		}
		else{
			beneficiario.setNombre(vistaCrearBeneficiario.txtNombre.getText());
			System.out.print(beneficiario.getNombre());
		}
		if(!validar.validarSoloLetras(vistaCrearBeneficiario.txtApellido.getText())){
			vistaCrearBeneficiario.txtApellido.setText("debe ingresar solo letras");
			flag = false;
		}
		else{
			beneficiario.setNombre(vistaCrearBeneficiario.txtApellido.getText());
			System.out.print(beneficiario.getApellido());
		}
		if(!validar.validarSoloNumeros(vistaCrearBeneficiario.txtCedula.getText())){
			vistaCrearBeneficiario.txtCedula.setText("Debe ingresar solo numeros");
			flag=false;
		}
		else{
			beneficiario.setCi(Integer.parseInt(vistaCrearBeneficiario.txtCedula.getText()));
			System.out.print(beneficiario.getCi());
		}

		return flag;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar ==e.getSource()){
			initializerView();
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			if(ValidarEmpleado(vistaCrear)){
				
				if (empleado.crearEmpleado()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
            		empleado.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));

				}
				else{
			  		System.out.println("adios");
				}
			}
			
		}
		if(this.vistaCrear.btnRegresar==e.getSource()){
			
			
		}
		if(this.vistaCrear.btnAgregarTelefonos==e.getSource()){
			
		}
		if(this.vistaCrear.btnAgregarRedes==e.getSource()){
			
		}
		if(this.vistaCrear.btnAgregarBeneficiarios==e.getSource()){
			initializerBeneficiarioCreateView();
			
		}
		if (this.vistaCrearBeneficiario.btnRegistrar==e.getSource()){
			if(ValidarBeneficiario(vistaCrearBeneficiario)){
					empleado.getBeneficiarios().add(beneficiario);
					System.out.println("hola");			  		
			  		vistaCrearBeneficiario.FrInicio.setVisible(false);
				
			}

		}
		
		
	}
	
	
	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
	        
	        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
	        int row = e.getY()/show.table.getRowHeight();
	        Empleado empleado = new Empleado();
	        
	        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
	            Object value = show.table.getValueAt(row, column);
	            if(value instanceof JButton){
	                ((JButton)value).doClick();
	                JButton boton = (JButton) value;

	                if(boton.getName().equals("m")){
	                    System.out.println("Click en el boton modificar");
	                    System.out.println(show.table.getValueAt(row, 0) );
	                    //EVENTOS MODIFICAR
	                }
	                if(boton.getName().equals("e")){
	                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Empleado?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
	                    System.out.println("Click en el boton eliminar");
	                    empleado.setCi(Integer.parseInt(show.table.getValueAt(row, 0).toString()));
	                    empleado.eliminarEmpleado();
	                    show.table.setModel(
	            		empleado.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));
	                    //EVENTOS ELIMINAR
	                }
	                if(boton.getName().equals("v")){
	                    System.out.println("Click en el boton ver");
	                    //EVENTOS MODIFICAR
	                }
	            }

	        }


	    }

}
