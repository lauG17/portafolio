package com.ucab.airucab.controllers;


import com.ucab.airucab.models.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.ucab.airucab.views.*; 

public class LoginController implements ActionListener {
	
	public LoginView vista;
	public Login login;
	private Validaciones validar;
	
	public LoginController() {
		this.vista = new LoginView();
		this.login = new Login();
		this.validar = new Validaciones();
		this.initializerView();
		
	}
	
	
	public void initializerView() {
		
		this.vista.btn_login.addActionListener(this);
		this.vista.FrInicio.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (this.vista.btn_login == e.getSource())	{	
		  	
			if (validarLogin()) {		  		 
			  	
		  		if(login.login()){
			  		
		  			int rol = login.getRol();
		  			vista.FrInicio.setVisible(false);
			  		if (rol == 1 )//ADMINISTRADOR 
			  			new DashboardController();
			  		if (rol == 2 )//CLIENTE 
			  			System.out.println("hola2");
			  		if (rol == 3 )//PROVEEDOR 
			  			System.out.println("hola3");
			  		if (rol == 4 )//SEDE 
			  			System.out.println("hola4");
			  		if (rol == 5 )//EMPLEADO 
			  			System.out.println("hola5");
			  	}
		  		else 
		  			JOptionPane.showMessageDialog(vista, "    Usuario invalido", "ERROR",JOptionPane.ERROR_MESSAGE, null);
		  	}
		}
		
	}

	@SuppressWarnings("deprecation")
	public boolean validarLogin() {
		boolean flag = true;
		
		if(!validar.validarSoloLetras(vista.text_id.getText())) {
			vista.text_id.setText("debe ingresar solo letras");
			flag = false;
		}else {
			this.login.setUsername(vista.text_id.getText());
		}
		if(!validar.validarSoloLetrasNumeros(vista.text_pass.getText())) {
			vista.text_pass.setText("debe ingresar solo letras o numeros");
			flag = false;
		}else {
			this.login.setPassword(vista.text_pass.getText());;
		}
		return flag;
	}
	
	

}
