package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import com.ucab.airucab.views.ClientCreateView;
import com.ucab.airucab.views.DashboardView;

public class DashboardController implements ActionListener {
	
	public DashboardView vista;
	public ClientCreateView clienteVista;
	
	public DashboardController(){
		//this.vista= new DashboardView();
		this.initializerView();
	}
	private void initializerView( ) {

		this.vista = new DashboardView();
		this.vista.btnReportes.addActionListener(this);
		this.vista.btnAviones.addActionListener(this);
		this.vista.btnClientes.addActionListener(this);
		this.vista.btnProveedores.addActionListener(this);
		this.vista.btnEnvios.addActionListener(this);
		this.vista.btnPruebas.addActionListener(this);
		this.vista.btnPieza.addActionListener(this);
		this.vista.btnMateriales.addActionListener(this);
		this.vista.btnPedidos.addActionListener(this);
		this.vista.btnEmpleados.addActionListener(this);
		this.vista.FrInicio.setVisible(true);
	}
	
	private void InitializerClienteVista(){
		new ClienteController();
	}
	private void initializePruebaVista(){
		new PruebaController();
	}
	private void InitializerProvedoresVista(){
		new ProveedorController();
	}
	private void initializerReportesvista() {
		new MenuViewController();
	}
	public void actionPerformed(ActionEvent e) {
		if (this.vista.btnAviones==e.getSource()){
			new AvionController();
		} 
		if(this.vista.btnClientes==e.getSource()){
			InitializerClienteVista();			
		} 
		if(this.vista.btnProveedores==e.getSource()){
			InitializerProvedoresVista();
		}
		if(this.vista.btnPruebas==e.getSource()){
			initializePruebaVista();
		}
		if(this.vista.btnPieza ==e.getSource()){
			new PiezaController();
		}
		if(this.vista.btnPedidos==e.getSource()){
			new PedidoController();
		}
		if(this.vista.btnMateriales==e.getSource()){
			new MaterialController();
		}
		if(this.vista.btnEnvios==e.getSource()){
			new SedeController();
		}
		if(this.vista.btnEmpleados==e.getSource()){
			new EmpleadoController();
		}
		if (this.vista.btnReportes==e.getSource()) {
			initializerReportesvista() ; 
		}
	
	}

}
