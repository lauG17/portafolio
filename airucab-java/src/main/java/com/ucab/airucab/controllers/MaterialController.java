package com.ucab.airucab.controllers;

import com.ucab.airucab.models.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.ucab.airucab.views.*;


public class MaterialController implements ActionListener {
		
		public MaterialCreateView vista;
		public Material material ;
		private Validaciones validar;
		
		public MaterialController() {
			
			this.material = new Material();
			this.validar = new Validaciones();
			this.initializerView();
			
		}
		
		
		public void initializerView() {
			this.vista = new MaterialCreateView();
			this.vista.agregar_btn.addActionListener(this);
			this.vista.FrInicio.setVisible(true);
		}
		
		
		
		
		public boolean validarMaterial() {
			boolean flag = true;
			System.out.println(vista.nombre_text.getText());
			if(!validar.validarSoloLetras(vista.nombre_text.getText())) {
				vista.nombre_text.setText("debe ingresar solo letras");
				flag = false;
			}else {
				this.material.setNombre(vista.nombre_text.getText());
			}

			return flag;
		}
		
		
		
		public void actionPerformed(ActionEvent e) {
			
			if (this.vista.agregar_btn == e.getSource())	{	
			  	if (validarMaterial()) {
			  
			  	if(material.crearMaterial()){
			  		//TODO poner controlador
			  		System.out.println("hola");
			  	}else {
			  	   //TODO poner label con error 
			  		System.out.println("adios");
			  	}
			  	
			}
			
		}

	}

		
}
