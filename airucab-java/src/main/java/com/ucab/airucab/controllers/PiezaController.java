package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import com.ucab.airucab.models.*;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.*;



public class PiezaController implements ActionListener {
	public PiezaCreateView vistaCrear;
	public Pieza pieza;
	public Validaciones validar;
	
	public PiezaController (){
	
		this.pieza = new Pieza();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	
	private void initializerView( ) {

		this.vistaCrear = new PiezaCreateView();		
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		
		vistaCrear = new PiezaCreateView();
		vistaCrear.btn_agregar_material.addActionListener(this);
		vistaCrear.table.setModel(pieza.tableShow(vistaCrear.col,vistaCrear.check_material));
		vistaCrear.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
	
		//this.show.btn_agregar.addActionListener(this);
	    this.vistaCrear.FrInicio.setVisible(true);
	}
	

	
	public boolean validarPieza(PiezaCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.text_nombre.getText())) {
			vistaCrear.text_nombre.setText("debe ingresar solo letras");
			flag = false;			
		}
		else {
			pieza.setNombre(vistaCrear.text_nombre.getText());
			System.out.println("nombre"+pieza.getNombre());
		}
		if (!validar.validarVacio(vistaCrear.text_modelo.getText())){
			vistaCrear.text_modelo.setText("Este campo no puede quedar vacio");
			flag =false;
		}
		else{
			pieza.setModelo(vistaCrear.text_modelo.getText());
			System.out.println("modelo"+pieza.getModelo());
		}
		if (!validar.validarSoloNumeros(vistaCrear.text_tiempo.getText())){
			System.out.println("timepooo"+vistaCrear.text_tiempo.getText());
			vistaCrear.text_tiempo.setText("Este campo solo acepta numeros");
			flag =false;
		}
		else{
			pieza.setDuraccion((Integer.parseInt(vistaCrear.text_tiempo.getText())));
			System.out.println("duracion"+pieza.getDuraccion());
		}
		if (!validar.validarSoloLetras(vistaCrear.text_descripcion.getText())){
			vistaCrear.text_descripcion.setText("Este campo no puede quedar vacio");
			flag =false;
		}
		else{
			pieza.setDescripcion((vistaCrear.text_descripcion.getText()));
			System.out.println("descripcion"+pieza.getDescripcion() );
		}
				
		
		return flag;
		
	}
	
	private void agregarMaterial() {
		for (int i = 0; i < vistaCrear.table.getRowCount(); i++) {
			Boolean chked = Boolean.valueOf(vistaCrear.table.getValueAt(i, 2)
					.toString());
			String dataCol1 = vistaCrear.table.getValueAt(i, 0).toString();
			if (chked) {
				pieza.getMateriales().add(new Material(Integer.parseInt(vistaCrear.table.getValueAt(i, 0).toString()),
						                             vistaCrear.table.getValueAt(i, 1).toString(),
						                             Integer.parseInt(vistaCrear.table.getValueAt(i, 3).toString())));
				JOptionPane.showMessageDialog(null, dataCol1);
				
			}  
		}
	}
	
	

	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (vistaCrear.btn_agregar_material == e.getSource()) {
			if (validarPieza(vistaCrear)) {
				agregarMaterial();
	
	
			if(!pieza.getMateriales().isEmpty() && pieza.crearPieza()) {
				
				System.out.println("hola");
				
			}else
				System.out.println("adios");
		}}
		
		
		
		/*if(this.show.btn_agregar ==e.getSource()){
			initializerView( );
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			if(validarCliente(vistaCrear)){
				
				if (cliente.crearCliente()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
            		cliente.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));

				}
				else{
			  		System.out.println("adios");
			}
			
		}
				}
		if(this.vistaCrear.btnRegresar==e.getSource()){
			
			
		}
		if(this.vistaCrear.btnAgregarTelefonos==e.getSource()){
			
		}*/

		
	}
	
    private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = vistaCrear.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/vistaCrear.table.getRowHeight();
        Pieza pieza = new Pieza();
        
        if(row < vistaCrear.table.getRowCount() && row >= 0 && column < vistaCrear.table.getColumnCount() && column >= 0){
            Object value = vistaCrear.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(vistaCrear.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Cliente?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    //pieza.setId(vistaCrear.table.getValueAt(row, 0).toString());  
                    //vistaCrear.table.setModel(
            		//Pieza.tableShow(vistaCrear.col,vistaCrear.check_material));
                    //EVENTOS ELIMINAR
                }
                if(boton.getName().equals("v")){
                    System.out.println("Click en el boton ver");
                    //EVENTOS MODIFICAR
                }
            }

        }


}


    
    
}