package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.airucab.models.Cliente;
import com.ucab.airucab.models.Sede;
import com.ucab.airucab.models.Validaciones;

import com.ucab.airucab.views.SedeCreateView;
import com.ucab.airucab.views.SedeShowView;


public class SedeController implements ActionListener {
	public SedeShowView show;
	public SedeCreateView vistaCrear;
	public Sede sede;
	public Validaciones validar;
	
	public SedeController(){
		this.sede=new Sede();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	
	private void initializerView( ) {

		this.vistaCrear = new SedeCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		
		this.vistaCrear.FrInicio.setVisible(true);
	}
	private void initializerShowView( ) {
		
		show = new SedeShowView();
		show.table.setModel(
				sede.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	public boolean validarSede(SedeCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.txtNombre.getText())) {
			vistaCrear.txtNombre.setText("debe ingresar solo letras");
			flag = false;
			
		}
		else {
			sede.setNombre(vistaCrear.txtNombre.getText());
			System.out.print(sede.getNombre());
		}
		if(!validar.validarSoloLetras(vistaCrear.txtTipo.getText())) {
			vistaCrear.txtTipo.setText("debe ingresar solo letras");
			flag = false;
			
		}
		else {
			sede.setNombre(vistaCrear.txtTipo.getText());
			System.out.print(sede.getNombre());
		}
		
		return flag;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar ==e.getSource()){
			initializerView( );
		}
		if(this.show.btn_ver==e.getSource()){
			
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			if(validarSede(vistaCrear)){
				if (sede.crearSede()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
            		sede.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));

				}
				else{
			  		System.out.println("adios");
				}
			}
			
			
		}		
	}
private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Cliente cliente = new Cliente();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar esta sede", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    sede.setCodigo(Integer.parseInt(show.table.getValueAt(row, 0).toString()));
                    sede.eliminarSede();
                    show.table.setModel(
            		cliente.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
                if(boton.getName().equals("v")){
                    System.out.println("Click en el boton ver");
                    //EVENTOS MODIFICAR
                }
            }

        }


    }

}
