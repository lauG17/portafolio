package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import com.ucab.airucab.models.Envio;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.*;



public class EnvioController implements ActionListener {
	public EnvioCreateView vistaCrear;
	public Envio envio;
	public Validaciones validar;
	
	public EnvioController(){
	
		this.envio = new Envio();
		this.validar = new Validaciones();
	}
	
	private void initializerView( ) {

		this.vistaCrear = new EnvioCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	

	
	public boolean validarEnvio(EnvioCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.txtStatusEnvio.getText())) {
			vistaCrear.txtStatusEnvio.setText("debe ingresar solo letras");
			flag = false;
			
		}
		else {
			envio.setStatusEnvio(vistaCrear.txtStatusEnvio.getText());
			System.out.print(envio.getStatusEnvio());
		}
		if (vistaCrear.calendar.getDate() == null){
			System.out.println("ingrese una fecha");
			flag = false;
		}else
			envio.getFecha().setFechaDate(new Date(vistaCrear.calendar.getDate().getTime()));
	
	if (vistaCrear.calendarF.getDate() == null){
		System.out.println("ingrese una fecha");
		flag = false;
	}else
		envio.getFecha().setFechaDate(new Date(vistaCrear.calendarF.getDate().getTime()));
    	
		return flag;
		
	}
	

	public Envio getEnvio() {
		return envio;
	}

	public void setEnvio(Envio envio) {
		this.envio = envio;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	

}