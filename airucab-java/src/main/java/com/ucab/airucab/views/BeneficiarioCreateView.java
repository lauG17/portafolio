package com.ucab.airucab.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


import javax.swing.JComboBox;

public class BeneficiarioCreateView extends JFrame {

	public JPanel contentPane;
	public JTextField txtNombre;

	public JFrame FrInicio;
	public JButton btnRegistrar;
	public JButton btnRegresar;
	public JComboBox <String> comboBox;
	
	private JLabel lblCedula;
	public JTextField txtCedula;
	public JTextField txtApellido;


	public  BeneficiarioCreateView()   {
		initializerView();
		
	}
	public void initializerView(){
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Beneficiario");
		FrInicio.setBounds(100, 100, 600, 406);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(12, 12, 553, 356);
		contentPane.setLayout(null);
		
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(411, 319, 117, 25);
		contentPane.add(btnRegistrar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 96, 70, 36);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(120, 96, 173, 38);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
	
	    btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(12, 319, 120, 25);
		contentPane.add(btnRegresar);
		
		
		FrInicio.getContentPane().add(contentPane);
		
		lblCedula = new JLabel("Cedula");
		lblCedula.setBounds(12, 37, 70, 33);
		contentPane.add(lblCedula);
		
		txtCedula = new JTextField();
		txtCedula.setText("Cedula");
		txtCedula.setBounds(120, 36, 173, 36);
		contentPane.add(txtCedula);
		txtCedula.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 167, 70, 36);
		contentPane.add(lblApellido);
		
		txtApellido = new JTextField();
		txtApellido.setText("Apellido");
		txtApellido.setBounds(120, 168, 173, 36);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		JLabel lblParentesco = new JLabel("Parentesco");
		lblParentesco.setBounds(12, 231, 97, 33);
		contentPane.add(lblParentesco);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(120, 235, 173, 24);
		comboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Seleccione","Madre","Padre","Herman@","Espos@","Hij@"}));
		contentPane.add(comboBox);
		FrInicio.setResizable(true);
		
	}
}
