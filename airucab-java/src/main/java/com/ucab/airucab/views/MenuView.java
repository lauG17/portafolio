package com.ucab.airucab.views;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class MenuView extends JFrame {

	private JPanel contentPane;
	public JFrame FrInicio;
	public JButton btnProdAnual;
	public JButton btnProdProduccion;
	public JButton btnMejorCliente;
	public JButton btnDescripcionPiezas;
	public JButton btnModeloAvion;
	public JButton btnCantMediaAviones;
	public JButton btnModeloVendido;
	public JButton btnFallosMaterial ;
	
	public JButton btnAlasUtilizadas;
	public JButton btnProductoMas;
	public JButton btnEspecificacionesDeModelo;
	public JButton btnCantidadDeProductos;
	public JButton btnProductoMasPedidoMaterial;
	public JButton btnListadoDeProveedores;
	public JButton btnFallospiezas;
	public JButton btnInventarioMensualPiezas;
	
	public JButton btnInventarioMensual;
	public JButton btnProductoMasPedidoPieza;
	public JButton btnProductoMasPedidoAvion;
	public MenuView() {
		initializerView();

	}
	public void initializerView(){
		FrInicio = new JFrame();
		FrInicio.setTitle("AirUcab");
		FrInicio.setBounds(100, 100, 682, 507);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 642, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(12, 12, 700, 467);
		contentPane.setLayout(null);
		
		btnProdAnual = new JButton("Produccion Anual");
		btnProdAnual.setBounds(293, 66, 249, 25);
		contentPane.add(btnProdAnual);
		
		btnProdProduccion = new JButton("Promedio de Produccion mensual");
		btnProdProduccion.setBounds(293, 177, 249, 25);
		contentPane.add(btnProdProduccion);
		
		btnMejorCliente = new JButton("topten Mejores Clientes ");
		btnMejorCliente.setBounds(12, 103, 233, 25);
		contentPane.add(btnMejorCliente);
		
		btnDescripcionPiezas = new JButton("Descripcion Piezas");
		btnDescripcionPiezas.setBounds(12, 30, 233, 25);
		contentPane.add(btnDescripcionPiezas);
		
		btnModeloAvion = new JButton("Modelos de Aviones");
		btnModeloAvion.setBounds(293, 140, 249, 25);
		contentPane.add(btnModeloAvion);
		
		btnCantMediaAviones = new JButton("Cantidad media de aviones producida mensualmente");
		btnCantMediaAviones.setBounds(12, 251, 530, 25);
		contentPane.add(btnCantMediaAviones);
		
		btnModeloVendido = new JButton("modelo mas vendido");
		btnModeloVendido.setBounds(12, 140, 233, 25);
		contentPane.add(btnModeloVendido);
				
		btnFallosMaterial = new JButton("Fallos de Material");
		btnFallosMaterial.setBounds(293, 103, 249, 25);
		contentPane.add(btnFallosMaterial);

		
		
		btnAlasUtilizadas = new JButton("tipo de alas mas utilizado");
		btnAlasUtilizadas.setBounds(12, 66, 233, 25);
		contentPane.add(btnAlasUtilizadas);
		
		btnInventarioMensual = new JButton("Inventario Mensual");
		btnInventarioMensual.setBounds(293, 30, 249, 25);
		contentPane.add(btnInventarioMensual);
		
		btnProductoMas = new JButton("Producto mas pedido al inventario ");
		btnProductoMas.setBounds(12, 288, 530, 25);
		contentPane.add(btnProductoMas);
		
		btnEspecificacionesDeModelo = new JButton("Especificaciones de modelo");
		btnEspecificacionesDeModelo.setBounds(12, 177, 233, 25);
		contentPane.add(btnEspecificacionesDeModelo);
		
		btnProductoMasPedidoMaterial = new JButton("material mas pedido");
		btnProductoMasPedidoMaterial.setBounds(12, 325, 530, 25);
		contentPane.add(btnProductoMasPedidoMaterial);
		
		btnListadoDeProveedores = new JButton("Listado de Proveedores ");
		btnListadoDeProveedores.setBounds(12, 214, 233, 25);
		contentPane.add(btnListadoDeProveedores);
		
		btnFallospiezas = new JButton("fallos piezas ");
		btnFallospiezas.setBounds(293, 214, 249, 25);
		contentPane.add(btnFallospiezas);
		
		btnProductoMasPedidoAvion = new JButton("avion mas pedido");
		btnProductoMasPedidoAvion.setBounds(12, 362, 530, 25);
		contentPane.add(btnProductoMasPedidoAvion);
		
		btnInventarioMensualPiezas = new JButton("Inventario mensual de piezas");
		btnInventarioMensualPiezas.setBounds(12, 399, 261, 25);
		contentPane.add(btnInventarioMensualPiezas);
		
		FrInicio.getContentPane().add(contentPane);
		
		btnProductoMasPedidoPieza = new JButton("Producto Mas pedido Pieza");
		btnProductoMasPedidoPieza.setBounds(293, 399, 249, 25);
		contentPane.add(btnProductoMasPedidoPieza);
	}
}