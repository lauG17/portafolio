package com.ucab.airucab.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class PruebaCreateView extends JFrame {

	public JPanel contentPane;
	public JFrame FrInicio;
	public JTextField nombre_text;
	public JButton agregar_btn;
	private JLabel lblDuracionPrueba;
	public JTextField textprueba;
	/**
	 * Launch the application.
	 */
	/**
	 * Create the frame.
	 */
	public PruebaCreateView() {
		InicialitationView();
	}
	public void InicialitationView() {
		FrInicio = new JFrame();
		FrInicio.setTitle("Nueva Prueba");
		FrInicio.setBounds(0, 0, 500, 500);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 0, 500, 500);
		contentPane.setLayout(null);
		
			
			
        nombre_text = new JTextField();
        nombre_text.setBounds(23, 98, 253, 20);
        nombre_text.setColumns(20);
        contentPane.add(nombre_text);
        
        
        agregar_btn = new JButton("Agregar");
        agregar_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
		agregar_btn.setBounds(302, 98, 114, 25);
		contentPane.add(agregar_btn);
        
    
        
        JLabel lblNombreDeMaterial = new JLabel("Nombre de la Prueba");
        lblNombreDeMaterial.setBounds(23, 73, 161, 14);
        contentPane.add(lblNombreDeMaterial);
        
		FrInicio.getContentPane().add(contentPane);
		
		lblDuracionPrueba = new JLabel("Duracion Prueba");
		lblDuracionPrueba.setBounds(23, 163, 149, 15);
		contentPane.add(lblDuracionPrueba);
		
		textprueba = new JTextField();
		textprueba.setBounds(23, 220, 114, 19);
		contentPane.add(textprueba);
		textprueba.setColumns(10);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.setResizable(false);
	}
}
