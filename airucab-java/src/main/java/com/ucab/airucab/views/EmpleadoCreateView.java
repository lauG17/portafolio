package com.ucab.airucab.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class EmpleadoCreateView extends JFrame {
	public JFrame FrInicio;
	public JTextField txtCi;
	public JTextField txtNombre;
	public JTextField txtApellido;
	public JTextField txtCargo;
	public JTextField txtTitulo;
	public JTextField txtDireccion;
	public JButton btnAgregarBeneficiarios;
	public JButton btnRegistrar;
	public JButton btnAgregarCorreos;
	public JButton btnRegresar;
	public JButton btnAgregarTelefonos;
	public JButton btnAgregarRedes;
	public JDateChooser calendar;
	public JComboBox comboBox;


	public EmpleadoCreateView() {
		InitializerView();
	}
	
	public void InitializerView(){

		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Empleado");
		FrInicio.setBounds(100, 100, 665, 446);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(12, 0, 630, 408);
		FrInicio.getContentPane().add(panel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 61, 80, 28);
		panel.add(lblNombre);
		
		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setBounds(12, 26, 80, 28);
		panel.add(lblCedula);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 101, 80, 28);
		panel.add(lblApellido);
		
		JLabel lblCargo = new JLabel("Cargo");
		lblCargo.setBounds(12, 136, 80, 28);
		panel.add(lblCargo);
		
		JLabel lblTitulo = new JLabel("Titulo");
		lblTitulo.setBounds(12, 176, 80, 28);
		panel.add(lblTitulo);
		
		txtCi = new JTextField();
		txtCi.setText("NroCedula");
		txtCi.setColumns(10);
		txtCi.setBounds(93, 31, 114, 19);
		panel.add(txtCi);

		
		txtNombre = new JTextField();
		txtNombre.setText("Nombre");
		txtNombre.setColumns(10);
		txtNombre.setBounds(93, 66, 114, 19);
		panel.add(txtNombre);
		
		txtApellido = new JTextField();
		txtApellido.setText("Apellido");
		txtApellido.setColumns(10);
		txtApellido.setBounds(93, 106, 114, 19);
		panel.add(txtApellido);
		
		txtCargo = new JTextField();
		txtCargo.setText("Cargo");
		txtCargo.setColumns(10);
		txtCargo.setBounds(93, 141, 114, 19);
		panel.add(txtCargo);
		
		txtTitulo = new JTextField();
		txtTitulo.setText("Titulo");
		txtTitulo.setColumns(10);
		txtTitulo.setBounds(93, 178, 114, 23);
		panel.add(txtTitulo);
		
		JLabel lblDireccin = new JLabel("Dirección");
		lblDireccin.setBounds(12, 266, 70, 28);
		panel.add(lblDireccin);
		
		txtDireccion = new JTextField();
		txtDireccion.setText("Dirección");
		txtDireccion.setColumns(10);
		txtDireccion.setBounds(93, 271, 114, 19);
		panel.add(txtDireccion);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(12, 313, 117, 25);
		panel.add(btnRegresar);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(474, 313, 117, 25);
		panel.add(btnRegistrar);
		
		btnAgregarTelefonos = new JButton("Agregar Telefonos");
		btnAgregarTelefonos.setBounds(293, 28, 237, 25);
		panel.add(btnAgregarTelefonos);
		
		btnAgregarCorreos = new JButton("Agregar Correos");
		btnAgregarCorreos.setBounds(293, 65, 237, 25);
		panel.add(btnAgregarCorreos);
		
		calendar = new JDateChooser();
		calendar.setBounds(460, 182, 131, 19);
		panel.add(calendar);
		
		btnAgregarRedes = new JButton("Agregar Redes");
		btnAgregarRedes.setBounds(293, 99, 237, 25);
		panel.add(btnAgregarRedes);
		
		btnAgregarBeneficiarios = new JButton("Agregar Beneficiarios");
		btnAgregarBeneficiarios.setBounds(293, 139, 237, 25);
		panel.add(btnAgregarBeneficiarios);
		
		JLabel lblAosDeServicio = new JLabel("Años de Servicio");
		lblAosDeServicio.setBounds(277, 182, 191, 28);
		panel.add(lblAosDeServicio);
		
		JDateChooser dateChooser_1 = new JDateChooser();
		dateChooser_1.setBounds(460, 248, 131, 19);
		panel.add(dateChooser_1);
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de Nacimiento");
		lblFechaDeNacimiento.setBounds(277, 252, 165, 15);
		panel.add(lblFechaDeNacimiento);
		
		comboBox = new JComboBox();
		comboBox.setBounds(93, 224, 114, 24);
		comboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Seleccione","","","","",""}));

		panel.add(comboBox);
		
		JLabel lblZonaDeTrabajo = new JLabel("Trabajo");
		lblZonaDeTrabajo.setBounds(5, 229, 87, 15);
		panel.add(lblZonaDeTrabajo);
		FrInicio.setResizable(true);
		
	}
}
