package com.ucab.airucab.views;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

@SuppressWarnings("serial")
public class LoginView extends JFrame {

	public JPanel contentPane;
	public JTextField text_id;
	public JTextField text_pass1;
	public JButton btn_login;
	public JFrame  FrInicio;
	public JPasswordField text_pass;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public LoginView() {
		initialization();
	}
	
	private void initialization() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Inicio de sesión");
		FrInicio.setBounds(0, 0, 250, 350);
		FrInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 0, 250, 350);
		contentPane.setLayout(null);
		
		
		text_id = new JTextField();
		text_id.setBounds(65, 142, 114, 19);
		contentPane.add(text_id);
		text_id.setColumns(10);
		
		text_pass = new JPasswordField();
		text_pass.setBounds(65, 173, 114, 19);
		contentPane.add(text_pass);
		text_pass.setColumns(10);
		
		btn_login = new JButton("Login");
		btn_login.setBounds(65, 217, 114, 25);
		contentPane.add(btn_login);
		
		FrInicio.getContentPane().add(contentPane);
		

		FrInicio.setResizable(false);
	}
}
