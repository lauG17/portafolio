package com.ucab.airucab.views;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.toedter.calendar.JDateChooser;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class EnvioCreateView extends JFrame {
	
	public JPanel contentPane;
	public JTextField txtStatusEnvio;
	public JFrame FrInicio;
	public JFrame FrFin;
	public JButton btnRegistrar;
	public JButton btnRegresar;
	public JDateChooser calendar;
	public JDateChooser calendarF;


	public  EnvioCreateView()   {
		initializerView();
		
	}
	public void initializerView(){
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Envio");
		FrInicio.setBounds(100, 100, 600, 406);
		FrInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(12, 12, 512, 332);
		contentPane.setLayout(null);
		
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRegistrar.setBounds(19, 224, 120, 25);
		contentPane.add(btnRegistrar);
		
		
		JLabel lblmonto = new JLabel("Estatus del Envio");
		lblmonto.setBounds(10, 21, 114, 19);
		contentPane.add(lblmonto);
		
		txtStatusEnvio = new JTextField();
		txtStatusEnvio.setText("Estatus Envio");
		txtStatusEnvio.setBounds(10, 37, 167, 19);
		contentPane.add(txtStatusEnvio);
		txtStatusEnvio.setColumns(40);
			
	    btnRegresar = new JButton("Regresar");
	    btnRegresar.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    	}
	    });
		btnRegresar.setBounds(19, 266, 120, 25);
		contentPane.add(btnRegresar);	
		
		
		calendar = new JDateChooser();
		calendar.setBounds(37, 133, 120, 19);
		contentPane.add(calendar);
				
		FrInicio.getContentPane().add(contentPane);
		FrInicio.setResizable(true);
		
		calendarF = new JDateChooser();
		calendarF.setBounds(273, 133, 120, 19);
		contentPane.add(calendarF);
				
		FrFin.getContentPane().add(contentPane);
		FrFin.setResizable(true);
		
	}
}
