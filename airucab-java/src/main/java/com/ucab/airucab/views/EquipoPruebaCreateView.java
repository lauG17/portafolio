package com.ucab.airucab.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

public class EquipoPruebaCreateView extends JFrame {
	public JFrame FrInicio;
	private JPanel contentPane;
	public JTable table;
	public JButton btnActualizar;
	public JButton btnCrearEquipo;
	public JComboBox comboBox;
    public JCheckBox check_material ;
	public DefaultTableModel tableModel;
    public Object[] col;
    public JTextField txtNombre;
    public JScrollPane jScrollPane;

	/**
	 * Create the frame.
	 */
	public EquipoPruebaCreateView() {
		initializerView();
		ver_tabla();
	}
	public void initializerView(){
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Equipo Prueba");
		FrInicio.setBounds(100, 100, 557, 344);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
	
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(12, 12, 533, 324);
		contentPane.setLayout(null);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(413, 267, 117, 25);
		contentPane.add(btnActualizar);
		
		btnCrearEquipo = new JButton("Crear Equipo");
		btnCrearEquipo.setBounds(12, 267, 130, 25);
		contentPane.add(btnCrearEquipo);
		
		//table = new JTable();
		//table.setBounds(195, 40, 497, 282);
		//contentPane.add(table);
		jScrollPane = new JScrollPane();
        table = new JTable();
		table.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {null, null, null, null}
	            },
	            new String [] {
	                "Title 1", "Title 2", "Title 3", "Title 4"}
	        ));
		
	    table.setBounds(28, 51, 382, 231);
	    
	    jScrollPane = new JScrollPane(table);
	    jScrollPane.setBounds(216, 25, 314, 218);
		contentPane.add(jScrollPane,BorderLayout.CENTER);
		
		JLabel lblNombreEquipo = new JLabel("Nombre Equipo");
		lblNombreEquipo.setBounds(12, 47, 136, 25);
		contentPane.add(lblNombreEquipo);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(12, 77, 114, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(12, 135, 94, 19);
		contentPane.add(dateChooser);
		
		JLabel lblFechaInicio = new JLabel("Fecha Inicio");
		lblFechaInicio.setBounds(12, 108, 94, 15);
		contentPane.add(lblFechaInicio);
		
		FrInicio.getContentPane().add(contentPane);
		

		
		comboBox = new JComboBox();
		comboBox.setBounds(12, 25, 171, 24);
		contentPane.add(comboBox);
		
		JLabel lblSedes = new JLabel("Sedes");
		lblSedes.setBounds(12, 12, 70, 15);
		contentPane.add(lblSedes);
		
	}
	   public void ver_tabla(){
		   check_material = new JCheckBox("c"); 
	       check_material.setName("c");
	       table.setDefaultRenderer(Object.class, new Render());
		   col = new Object[]{"Cedula","Nombre","Apellido","Cargo","Seleccionar"};
		   tableModel = new DefaultTableModel(new Object [][] {},col)
		   {
			   public boolean isCellEditable(int row, int column){
				   return false;
			   }
			   public Class<?> getColumnClass(int column){
       			switch (column) {
       			case 0:
       				return String.class;
       			case 1:
       				return String.class;
       			case 2:
       				return String.class;
       			case 3:
       				return String.class;
       			case 4:
       				return Boolean.class;

       			default:
           			return String.class;	
       			}
       			
       	};
		   };
		   table.setModel(tableModel);
		   table.setPreferredScrollableViewportSize(table.getPreferredSize());
	   }
}
