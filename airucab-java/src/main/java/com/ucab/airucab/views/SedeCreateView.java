package com.ucab.airucab.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

public class SedeCreateView extends JFrame {


	public JPanel contentPane;
	public JTextField txtNombre;
	public JTextField txtDireccion;
	public JFrame FrInicio;
	public JButton btnRegistrar;
	public JTextField txtTipo;
	public JButton btnRegresar;
	
	public  SedeCreateView()   {
		initializerView();
		
	}
	public void initializerView(){
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nueva SEDE");
		FrInicio.setBounds(100, 100, 373, 400);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(12, 12, 349, 337);
		contentPane.setLayout(null);
		
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(204, 273, 117, 25);
		contentPane.add(btnRegistrar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 27, 70, 15);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(22, 44, 114, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblDireccin = new JLabel("Dirección");
		lblDireccin.setBounds(22, 134, 70, 15);
		contentPane.add(lblDireccin);
		
		txtDireccion = new JTextField();
		txtDireccion.setText("direccion");
		txtDireccion.setBounds(22, 161, 114, 19);
		contentPane.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		
				
		FrInicio.getContentPane().add(contentPane);
		
		txtTipo = new JTextField();
		txtTipo.setText("Tipo");
		txtTipo.setBounds(22, 103, 114, 19);
		contentPane.add(txtTipo);
		txtTipo.setColumns(10);
		
		JLabel lblTipoSede = new JLabel("Tipo Sede");
		lblTipoSede.setBounds(22, 79, 70, 15);
		contentPane.add(lblTipoSede);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(12, 273, 117, 25);
		contentPane.add(btnRegresar);
		FrInicio.setResizable(true);
		
	}
}
