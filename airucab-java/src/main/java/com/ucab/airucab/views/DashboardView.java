package com.ucab.airucab.views;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class DashboardView extends JFrame {

	private JPanel contentPane;
	public JFrame FrInicio;
	public JButton btnAviones;
	public JButton btnPedidos;
	public JButton btnPieza;
	public JButton btnEnvios;
	public JButton btnProveedores;
	public JButton btnPruebas;
	public JButton btnClientes;
	public JButton btnMateriales;
	public JButton btnCerrarSesion;
	public JButton btnEmpleados;
	public JButton btnReportes;
	public DashboardView() {
		initializerView();

	}
	public void initializerView(){
		FrInicio = new JFrame();
		FrInicio.setTitle("AirUcab");
		FrInicio.setBounds(100, 100, 600, 406);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 642, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(101, 12, 399, 364);
		contentPane.setLayout(null);
		
		btnAviones = new JButton("Aviones");
		btnAviones.setBounds(191, 185, 179, 25);
		contentPane.add(btnAviones);
		
		btnPieza = new JButton("pieza");
		btnPieza.setBounds(12, 143, 167, 25);
		contentPane.add(btnPieza);
		
		btnPedidos = new JButton("pedidos");
		btnPedidos.setBounds(191, 30, 179, 25);
		contentPane.add(btnPedidos);
		
		btnEnvios = new JButton("sedes");
		btnEnvios.setBounds(12, 103, 167, 25);
		contentPane.add(btnEnvios);
		
		btnProveedores = new JButton("Proveedores");
		btnProveedores.setBounds(12, 30, 167, 25);
		contentPane.add(btnProveedores);
		
		btnPruebas = new JButton("Pruebas");
		btnPruebas.setBounds(12, 185, 167, 25);
		contentPane.add(btnPruebas);
		
		btnClientes = new JButton("Clientes");
		btnClientes.setBounds(12, 67, 167, 25);
		contentPane.add(btnClientes);
		
		btnMateriales = new JButton("Materiales");
		btnMateriales.setBounds(191, 143, 179, 25);
		contentPane.add(btnMateriales);
				
		//btnCerrarSesion = new JButton("Cerrar sesion");
		//btnCerrarSesion.setBounds(133, 310, 140, 25);
		//contentPane.add(btnCerrarSesion);

		FrInicio.getContentPane().add(contentPane);
		
		btnEmpleados = new JButton("Empleados");
		btnEmpleados.setBounds(191, 67, 179, 25);
		contentPane.add(btnEmpleados);
		
		btnReportes = new JButton("Reportes");
		btnReportes.setBounds(191, 103, 179, 25);
		contentPane.add(btnReportes);
	}
}
