package com.ucab.airucab.views;



import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class PedidoCreateView extends JFrame {



	public JPanel contentPane;
	public JComboBox comboBox;
	public JButton btn_agregar;
	public JFrame  FrInicio;
	public JTable tableCliente;
	public JTable tableAvion;
    public JScrollPane scrollCliente;
    public JScrollPane scrollAvion;
    public DefaultTableModel tableModelAvion;
    public DefaultTableModel tableModelCliente;
    public Object[] col;
    public JCheckBox check_material ;
    public JTextField text_pago;


	public PedidoCreateView() {
		initialization();
		ver_tablaCliente();
		ver_tablaAvion();
	}
	
	private void initialization() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Crear Pedido");
		FrInicio.setBounds(0, 0, 600, 328);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE );
		FrInicio.getContentPane().setLayout(null);
				
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 0, 600, 328);
		contentPane.setLayout(null);
		
		btn_agregar = new JButton("Agregar");
		btn_agregar.setBounds(250, 231, 236, 25);
		contentPane.add(btn_agregar);
		
		scrollCliente = new JScrollPane();
	    tableCliente = new JTable();
	    tableCliente.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {null, null, null, null}
	            },
	            new String [] {
	                "Title 1", "Title 2", "Title 3", "Title 4"}
	        ));
		
	    tableCliente.setBounds(28, 51, 382, 231);
	    
	    scrollCliente = new JScrollPane(tableCliente);
	    scrollCliente.setBounds(30, 62, 236, 157);
		contentPane.add(scrollCliente,BorderLayout.CENTER);
		
		scrollAvion = new JScrollPane();
		tableAvion = new JTable();
		tableAvion.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {null, null, null, null}
	            },
	            new String [] {
	                "Title 1", "Title 2", "Title 3", "Title 4"}
	        ));
		
		scrollAvion.setBounds(28, 51, 382, 231);

	    
		scrollAvion = new JScrollPane(tableAvion);
	    scrollAvion.setBounds(300, 62, 236, 157);
		contentPane.add(scrollAvion,BorderLayout.CENTER);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(30, 231, 173, 24);
		comboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Tipo de pago","Efectivo","Cheque","Debito","Credito"}));
		contentPane.add(comboBox);
		
		FrInicio.getContentPane().add(contentPane);
		
		text_pago = new JTextField();
		text_pago.setBounds(30, 265, 173, 19);
		text_pago.setColumns(10);
		contentPane.add(text_pago);
		
	    FrInicio.setResizable(false);
	}
	
	   public void ver_tablaAvion(){
	        check_material = new JCheckBox("c"); 
	        check_material.setName("c");
	        tableCliente.setDefaultRenderer(Object.class, new Render());
	        col = new Object[]{"id","Nombre","modelo","select"};
	        tableModelAvion = new DefaultTableModel(new Object [][] {},col)
	        {
	            public boolean isCellEditable(int row, int column){
	                return false;
	            }

	        		public Class<?> getColumnClass(int column){
	        			switch (column) {
	        			case 0:
	        				return String.class;
	        			case 1:
	        				return String.class;
	        			case 2:
	        				return String.class;
	        			case 3:
	        				return Boolean.class;
	        			default:
	            			return String.class;	
	        			}
	        			
	        	};
	        };

	  

	    }
	   
	   public void ver_tablaCliente(){
	        check_material = new JCheckBox("c"); 
	        check_material.setName("c");
	        tableCliente.setDefaultRenderer(Object.class, new Render());
	        col = new Object[]{"id","Nombre","select"};
	        tableModelCliente = new DefaultTableModel(new Object [][] {},col)
	        {
	            public boolean isCellEditable(int row, int column){
	                return false;
	            }

	        		public Class<?> getColumnClass(int column){
	        			switch (column) {
	        			case 0:
	        				return String.class;
	        			case 1:
	        				return String.class;
	        			case 2	:
	        				return Boolean.class;
	        			default:
	            			return String.class;	
	        			}
	        			
	        	};
	        };

	  

	    }
}
