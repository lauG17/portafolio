package com.ucab.airucab.models;

import java.util.Date;

import com.ucab.airucab.dao.FacturaDao;

public class Factura {

	private int tipoPago;
	private int pago;
	private Date fecha;
	private String fechaString; 
	private int status;
	private int id;
	private FacturaDao dao;

	
	public Factura() {
		dao = new FacturaDao();
	}
	
	
	public boolean create() {
		   dao.create(this);
		   if (status>0)
			   return true;
		   else 
			   return false;
			   
		}

	public int getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(int tipoPago) {
		this.tipoPago = tipoPago;
	}

	public int GetPago() {
		return pago;
	}

	public void setPago(int pago) {
		this.pago = pago;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFechaString() {
		return fechaString;
	}

	public void setFechaString(String fechaString) {
		this.fechaString = fechaString;
	}

}
