package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.BeneficiarioDao;

public class Beneficiario {
	
	private int ci ;
	private String nombre;
	private String apellido;
	private String parentezco;
	private int status;
	
	private BeneficiarioDao dao;
	
	public Beneficiario(){
		
		this.status = -1;
		this.dao = new BeneficiarioDao();
	}
	
	public boolean crearBeneficiario(){
	//	fecha.convertirDateString(fecha.getFechaDate());;
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public void modificarBeneficiario(){
		dao.update(this);

	}

	public ArrayList<Beneficiario> mostrarBeneficiario(){
		ArrayList<Beneficiario> beneficiarios = dao.readAll();
		return beneficiarios;
	}


	public boolean eliminarBeneficiario(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar, JButton ver){
    	ArrayList<Beneficiario>  beneficiarios = mostrarBeneficiario();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Beneficiario beneficiario : beneficiarios) {
    		tableModel.addRow(new Object[]{beneficiario.getCi(),beneficiario.getNombre(),modificar,eliminar,ver});
    	}
    	return tableModel;
    }	

	public int getCi() {
		return ci;
	}

	public void setCi(int ci) {
		this.ci = ci;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getParentezco() {
		return parentezco;
	}

	public void setParentezco(String parentezco) {
		this.parentezco = parentezco;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
