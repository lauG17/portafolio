package com.ucab.airucab.models;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.table.DefaultTableModel;
import com.ucab.airucab.dao.*;


public class Pieza {
	
	private PiezaDao dao;
	private Material material;
	private int status;
	private String descripcion;
	private int duraccion;
	private int id;
	private String modelo;
	private String nombre;
	private ArrayList<Material> materiales;
	private ArrayList<Prueba> pruebas;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	public Pieza() {
		// TODO Auto-generated constructor stub
		this.dao= new PiezaDao();
		materiales = new ArrayList<Material>(); 
	}
	
	public void llenarMaterialPiezas(ArrayList<Pieza> piezas) {
		System.out.println("llenarMaterialPiezas");
		for (Pieza pieza : piezas) {
			System.out.println("for"+pieza.getId());
			dao.rellenarMaterial(pieza);
		}

	}
	
	public boolean pedidoProveedor(ArrayList<Pieza> piezas ,int pedido){
		for (Pieza pieza : piezas) {
			System.out.println("pedido pieza "+ pieza.getId());
			dao.pedidoPieza(pieza,pedido);
		}	
		if (status>0)
			return true;
		else 
			return false;
		
	}
	
			
	public ArrayList<Material> mostrarMaterial(){
		ArrayList<Material> materiales = dao.readAll();
		return materiales;
	}
	
	public ArrayList<Prueba> mostrarPrueba(){
		ArrayList<Prueba> pruebas = dao.readAllPrueba();
		return pruebas;
	}

	public boolean crearPieza(){
		 
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}
	


    public DefaultTableModel tableShow(Object[] col, JCheckBox seleccionar){
    	ArrayList<Material>  materiales = mostrarMaterial();
    	col = new Object[]{"id","Nombre","duracion","select"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col) {
    		public Class<?> getColumnClass(int column){
    			switch (column) {
    			case 0:
    				return String.class;
    			case 1:
    				return String.class;
    			case 2:
    				return Boolean.class;
    			case 3:
    				return String.class;
    			default:
        			return String.class;	
    			}
    			   			
    	} 

    	};
    	
    	for (Material material : materiales) {
    		tableModel.addRow(new Object[]{material.getId(),material.getNombre(),false});
    	}
    	
    	return tableModel;
    }
    
    public DefaultTableModel tableShowPrueba(Object[] col, JCheckBox seleccionar){
    	ArrayList<Prueba>  pruebas = mostrarPrueba();
    	col = new Object[]{"Codigo","Nombre","duracion","Acciones"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col) {
    		public Class<?> getColumnClass(int column){
    			switch (column) {
    			case 0:
    				return String.class;
    			case 1:
    				return String.class;
    			case 2:
    				return String.class;
    			case 3:
    				return Boolean.class;
    			default:
        			return String.class;	
    			}
    			   			   			
    	} 

    	};
    	
    	for (Prueba prueba : pruebas) {
    		tableModel.addRow(new Object[]{prueba.getId(),prueba.getNombre(),prueba.getTiempo(),false});
    	}
    	
    	return tableModel;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Material> getMateriales() {
		return materiales;
	}

	public void setMateriales(ArrayList<Material> materiales) {
		this.materiales = materiales;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getDuraccion() {
		return duraccion;
	}

	public void setDuraccion(int duraccion) {
		this.duraccion = duraccion;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public ArrayList<Prueba> getPruebas() {
		return pruebas;
	}

	public void setPruebas(ArrayList<Prueba> pruebas) {
		this.pruebas = pruebas;
	}

}
