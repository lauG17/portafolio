package com.ucab.airucab.models;

import com.ucab.airucab.dao.PruebaDao;

public class Prueba {
	private int id;
	private int status; 
	private String nombre;
	private int tiempo;
    private PruebaDao dao;
	
	public Prueba() {
		this.dao = new PruebaDao();	   	
	}
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean crearPrueba() {
		
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public int getTiempo() {
		return tiempo;
	}
	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

}

