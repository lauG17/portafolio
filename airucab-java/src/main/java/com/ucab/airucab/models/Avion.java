package com.ucab.airucab.models;

import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.table.DefaultTableModel;
import com.ucab.airucab.dao.AvionDao;
import com.ucab.airucab.dao.PiezaDao;

public class Avion {

	private AvionDao dao;
	private Pieza pieza;
	private String nombre;
	private int status;
	private String tipo;
	private ArrayList<Pieza> piezas;
	private int longitud;
	private int id;
	private int capacidad;
	private String modelo;
	private int altura;

	
	public Avion() {
		
		this.dao= new AvionDao();
		pieza = new Pieza();
		piezas = new ArrayList<Pieza>(); 
	}
	
	public ArrayList<Pieza> mostrarPieza(){
		ArrayList<Pieza> piezas = dao.readAll();
		return piezas;
	}

	public boolean crearAvion(){
		 
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}
	
	public boolean llenarPiezas() {
		dao.llenarPieza(this);
		pieza.llenarMaterialPiezas(piezas);
		if (status>0)
			return true;
		else 
			return false;
	}
	
	public DefaultTableModel tableShow(Object[] col){
    	ArrayList<Pieza>  piezas = mostrarPieza();
    	col = new Object[]{"id","Nombre","Descripcion","duracion","select"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col) {
    		public Class<?> getColumnClass(int column){
    			switch (column) {
    			case 0:
    				return String.class;
    			case 1:
    				return String.class;
    			case 2:
    				return String.class;
    			case 3:
    				return String.class;
    			case 4:
    				return Boolean.class;
    			default:
        			return String.class;	
    			}
    			
    			
    			
    	} 

    	};
    	
    	for (Pieza pieza : piezas) {
    		tableModel.addRow(new Object[]{pieza.getId(),pieza.getNombre(),pieza.getDescripcion(),pieza.getDuraccion() ,false});
    	}
    	
    	return tableModel;
    }
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Pieza getPieza() {
		return pieza;
	}

	public void setPieza(Pieza pieza) {
		this.pieza = pieza;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public ArrayList<Pieza> getPiezas() {
		return piezas;
	}

	public void setPiezas(ArrayList<Pieza> piezas) {
		this.piezas = piezas;
	}


	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	

	
	}
