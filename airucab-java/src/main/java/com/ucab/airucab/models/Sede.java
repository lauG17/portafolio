package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.SedeDao;

public class Sede {
	private int codigo;
	private String nombre;
	private String tipo;
	private int status;

	private SedeDao dao;
	
	public Sede(){
		this.status = -1;
		this.dao = new SedeDao();
		
	}
	public boolean crearSede(){
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;
	}
	public void modificarSede(){
		dao.update(this);
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public boolean eliminarSede(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public ArrayList<Sede> mostrarSede(){
		ArrayList<Sede> sedes = dao.readAll();
		return sedes;
	}
	public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar, JButton ver){
    	ArrayList<Sede>  sedes = mostrarSede();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Sede sede : sedes) {
    		tableModel.addRow(new Object[]{sede.getCodigo(),sede.getNombre(),modificar,eliminar,ver});
    	}
    	return tableModel;
    }	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
