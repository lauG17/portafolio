package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.ProveedorDao;

public class Proveedor extends Empresa {
	

	private ProveedorDao dao;
    private Material material;

	public Proveedor(){
        super();
		this.dao = new ProveedorDao();
		material = new Material();
	}

	public boolean crearProveedor(){
		
		fecha.convertirDateString(fecha.getFechaDate());; 
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}
	
	public boolean pedidoProveedor(ArrayList<Pieza> piezas) {
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>(); 
		for (Pieza pieza : piezas) {
			for (Material material : pieza.getMateriales()) {
				proveedores.add(dao.proveedorMaterial(material));
			}
		}		
		for (Proveedor proveedor : proveedores) {
			dao.pedidoProveedor(proveedor);
		}
		
		if (status>0)
			return true;
		else 
			return false;
	}
	
	public void modificarProveedor(){
		dao.update(this);		
		
	}
	
	public void buscarProveedor(){
		
		
	}
	public ArrayList<Proveedor> mostrarProveedor(){
		ArrayList<Proveedor> proveedores = dao.readAll();
		return proveedores;
	}
	
	public boolean eliminarProveedor(){
		dao.delete(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}
	public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Proveedor>  proveedores = mostrarProveedor();
    	col = new Object[]{"Codigo","Nombre","fecha","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Proveedor proveedor : proveedores) {
    		tableModel.addRow(new Object[]{proveedor.getRif(),proveedor.getNombre(),proveedor.getFecha().getFechaString() ,modificar,eliminar});
    	}
    	return tableModel;
    }

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	

}
 