package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.EquipoPruebaDao;
import com.ucab.airucab.dao.PiezaDao;

public class EquipoPrueba {
	private int status;
	private int codigo;
	private String nombre;
	private Fecha fechainicio;
	private Fecha fechafin;
	private EquipoPruebaDao dao;

	private ArrayList<Empleado> empleados;
	
	public EquipoPrueba(){
		this.dao=new EquipoPruebaDao();
		empleados=new ArrayList<Empleado>();
	}
	public ArrayList<Empleado> MostrarEmpleado(){
		ArrayList<Empleado> empleados =dao.mostrarEmpleados();
		
		return empleados;
	}
	public ArrayList<Sede> MostrarSede(){
		ArrayList<Sede> sedes =dao.MostrarSedes();
		
		return sedes;
	}
	public boolean crearEquipoPrueba(){
		 
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}
	
	 public DefaultTableModel tableShow(Object[] col, JCheckBox seleccionar){
	    	ArrayList<Empleado>  empleados = MostrarEmpleado();
	    	col = new Object[]{"Cedula","Nombre","Apellido","Cargo","Seleccionar"};
	    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col) {
	    		public Class<?> getColumnClass(int column){
	    			switch (column) {
	    			case 0:
	    				return String.class;
	    			case 1:
	    				return String.class;
	    			case 2:
	    				return String.class;
	    			case 3:
	    				return String.class;
	    			case 4:
	    				return Boolean.class;
	    			default:
	        			return String.class;	
	    			}
	    			
	    			
	    			
	    	} 

	    	};
	    	
	    	for (Empleado empleado : empleados) {
	    		tableModel.addRow(new Object[]{empleado.getCi(),empleado.getNombre(),empleado.getApellido(),empleado.getCargoempleado(),false});
	    	}
	    	
	    	return tableModel;
	    }

	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Fecha getFechainicio() {
		return fechainicio;
	}
	public void setFechainicio(Fecha fechainicio) {
		this.fechainicio = fechainicio;
	}
	public Fecha getFechafin() {
		return fechafin;
	}
	public void setFechafin(Fecha fechafin) {
		this.fechafin = fechafin;
	}

}
