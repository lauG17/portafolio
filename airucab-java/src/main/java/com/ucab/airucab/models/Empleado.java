package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.EmpleadoDao;

public class Empleado {
	
	private int ci ;
	private String nombre;
	private String apellido;
	private Fecha fecha;
	private int direccion;
	private String titulo;
	private int tiemservicio;
	private String cargoempleado;
	private Beneficiario beneficiario;
	private int sede; 
	
	private ArrayList<Beneficiario> beneficiarios;
	
	public Empleado(){
		
		this.status = -1;
		this.fecha = new Fecha();
		this.dao= new EmpleadoDao();
		beneficiarios = new ArrayList<Beneficiario>();
	}

	

	private int status;

	private EmpleadoDao dao;
	
	public ArrayList<Beneficiario> mostrarBeneficiarios(){
		ArrayList<Beneficiario> beneficiarios =dao.readAllBeneficiario();
		
		
		return beneficiarios;
		
	}
	public ArrayList<Sede> MostrarSede(){
		ArrayList<Sede> sedes =dao.MostrarSedes();
		
		return sedes;
	}
	
	public boolean crearEmpleado(){
		fecha.convertirDateString(fecha.getFechaDate());;
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public void modificarEmpleado(){
		dao.update(this);

	}

	public ArrayList<Empleado> mostrarEmpleado(){
		ArrayList<Empleado> empleados = dao.readAll();
		return empleados;
	}
	
	

	public boolean eliminarEmpleado(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
	
	
	
    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar, JButton ver){
    	ArrayList<Empleado>  empleados = mostrarEmpleado();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Empleado empleado : empleados) {
    		tableModel.addRow(new Object[]{empleado.getCi(),empleado.getNombre(),modificar,eliminar,ver});
    	}
    	return tableModel;
    }	

	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public int getTiemservicio() {
		return tiemservicio;
	}


	public void setTiemservicio(int tiemservicio) {
		this.tiemservicio = tiemservicio;
	}


	public String getCargoempleado() {
		return cargoempleado;
	}


	public void setCargoempleado(String cargoempleado) {
		this.cargoempleado = cargoempleado;
	}

	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getCi() {
		return ci;
	}

	public void setCi(int ci) {
		this.ci = ci;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getDireccion() {
		return direccion;
	}

	public void setDireccion(int direccion) {
		this.direccion = direccion;
	}

	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		this.fecha = fecha;
	}
	public ArrayList<Beneficiario> getBeneficiarios() {
		return beneficiarios;
	}
	public void setBeneficiarios(ArrayList<Beneficiario> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
	public Beneficiario getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}
	public int getSede() {
		return sede;
	}
	public void setSede(int sede) {
		this.sede = sede;
	}
	
	


}