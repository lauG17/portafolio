package com.ucab.airucab.models;

import com.ucab.airucab.dao.EvaluacionDao;

public class Evaluacion {
	private int id;
	private int status; 
	private String nombre;
    private EvaluacionDao dao;
	
	public Evaluacion() {
		this.dao = new EvaluacionDao();	   	
	}
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean crearEvaluacion() {
		
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
