package com.ucab.airucab.models;

import java.util.ArrayList;

import com.ucab.airucab.dao.EnvioDao;




public class Envio  {
		
		private String statusEnvio ;
		private int status;
		private Fecha fecha;
		private int codigo;
		private EnvioDao dao;
		private int MontoAcreditado;
		private Envio envio; 
		
		public Envio(){
			this.dao = new EnvioDao();
		}
		

		public boolean crearEnvio(){
			fecha.convertirDateString(fecha.getFechaDate());;
			dao.create(this);
			if (status>0)
				return true;
			else
				return false;
	
		}
	
		public void modificarEnvio(){
			dao.update(this);
	
		}
	
		public void buscarEnvio(){
	
	
		}
	
	
		public ArrayList<Envio> mostrarEnvio(){
			ArrayList<Envio> envios = dao.readAll();
			return envios;
		}
	
	
		public boolean eliminarEnvio(){
			dao.delete(this);
			if (status>0)
				return true;
			else
				return false;
	
		}
		
		
		public int getCodigo() {
			return codigo;
		}
	
		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}
	
			
		public Envio getEnvio() {
				return envio;
			}
	
			public void setEnvio(Envio envio) {
				this.envio = envio;
			}
			
		
		public int getStatus() {
			return status;
		}
	
		public void setStatus(int status) {
			this.status = status;
		}
		
		public String getStatusEnvio() {
			return statusEnvio;
		}
	
		public void setStatusEnvio(String statusEnvio) {
			this.statusEnvio = statusEnvio;
		}
		
		public Fecha getFecha() {
			return fecha;
		}
	
		public void setFecha(Fecha fecha) {
			this.fecha = fecha;
		}


		public int getMontoAcreditado() {
			return MontoAcreditado;
		}


		public void setMontoAcreditado(int montoAcreditado) {
			MontoAcreditado = montoAcreditado;
		}



}
