package com.ucab.airucab.models;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.*;


public class Pedido {
	
	private PiezaDao daoPieza;
	private AvionDao daoAvion;
	private ClienteDao daoCliente;
	private Pieza pieza;
	private int status;
	private String descripcion;
	private int duraccion;
	private int id;
	private int cliente;
	private String modelo;
	private Proveedor proveedor;
	private String nombre;
	private Factura factura;
	private PedidoDao daoPedido;
	private Avion avion;
	

	
	public Pedido() {
		
		daoPieza = new PiezaDao();
		daoCliente = new ClienteDao();
		daoAvion = new AvionDao();
		proveedor = new Proveedor();
		daoPedido = new PedidoDao();
		factura = new Factura();
		avion = new Avion();
		
		}
	    
		
	public ArrayList<Pieza> mostrarPieza(){
		ArrayList<Pieza> piezas = daoPieza.readAllPieza();
		return piezas;
		}
		
	public ArrayList<Cliente> mostrarCliente(){
		ArrayList<Cliente> clientes = daoCliente.readAll();
		return clientes;
		}
		
	public ArrayList<Avion> mostrarAvion(){
		ArrayList<Avion> aviones = daoAvion.readAllAvion();
		return aviones;
		}

	public boolean create(){
		daoPedido.create(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}
	


    public DefaultTableModel tableShowCliente(Object[] col){
    	ArrayList<Cliente>  clientes = mostrarCliente();
        col = new Object[]{"Rif","Nombre","select"};
        DefaultTableModel tableModelCliente = new DefaultTableModel(new Object [][] {},col)
        {


        		public Class<?> getColumnClass(int column){
        			switch (column) {
        			case 0:
        				return String.class;
        			case 1:
        				return String.class;
        			case 2	:
        				return Boolean.class;
        			default:
            			return String.class;	
        			}
        			
        	};
        };
    	for (Cliente cliente : clientes) {
    		tableModelCliente.addRow(new Object[]{cliente.getRif(),cliente.getNombre(),false});
    	}
    	return tableModelCliente;
    }
    
    public DefaultTableModel tableShowAviones(Object[] col){
    	ArrayList<Avion>  aviones = mostrarAvion();
    	col = new Object[]{"id","Nombre","modelo","select"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col) {
    		public Class<?> getColumnClass(int column){
    			switch (column) {
    			case 0:
    				return String.class;
    			case 1:
    				return String.class;
    			case 2:
    				return String.class;
    			case 3:
    				return Boolean.class;
    			default:
        			return String.class;	
    			}
    			   			   			
    	} 

    	};
    	
    	for (Avion avion : aviones) {
    		tableModel.addRow(new Object[]{avion.getId(),"","",false});
    	}
    	
    	return tableModel;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}
	
	public int getStatus() {
		return status;
	}
		
	public void setStatus(int status) {
		this.status = status;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getDuraccion() {
		return duraccion;
	}

	public void setDuraccion(int duraccion) {
		this.duraccion = duraccion;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}



	public Pieza getPieza() {
		return pieza;
	}



	public void setPieza(Pieza pieza) {
		this.pieza = pieza;
	}



	public int getCliente() {
		return cliente;
	}



	public void setCliente(int cliente) {
		this.cliente = cliente;
	}


	public Avion getAvion() {
		return avion;
	}



	public void setAvion(Avion avion) {
		this.avion = avion;
	}


	public Proveedor getProveedor() {
		return proveedor;
	}


	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}


	public Factura getFactura() {
		return factura;
	}


	public void setFactura(Factura factura) {
		this.factura = factura;
	}

}
