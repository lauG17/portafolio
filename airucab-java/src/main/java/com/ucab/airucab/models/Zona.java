package com.ucab.airucab.models;

import com.ucab.airucab.dao.ZonaDao;

public class Zona {
	
		private int id;
		private int status; 
		private String nombre;
	    private ZonaDao dao;

		public Zona() {
	      this.dao = new ZonaDao();	   	
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		public boolean crearZona() {
			
			dao.create(this);
			if (status>0)
				return true;
			else 
				return false;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
}