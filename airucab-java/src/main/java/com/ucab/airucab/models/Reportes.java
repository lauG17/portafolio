package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.ReportesDao;

public class Reportes {

	private ReportesDao dao;
	private ArrayList<String> reportes;
	private String dato;
	public String getDato() {
		return dato;
	}

	public void setDato(String dato) {
		this.dato = dato;
	}

	public String getDato1() {
		return dato1;
	}

	public void setDato1(String dato1) {
		this.dato1 = dato1;
	}

	public String getDato2() {
		return dato2;
	}

	public void setDato2(String dato2) {
		this.dato2 = dato2;
	}

	public String getDato3() {
		return dato3;
	}

	public void setDato3(String dato3) {
		this.dato3 = dato3;
	}

	public String getDato4() {
		return dato4;
	}

	public void setDato4(String dato4) {
		this.dato4 = dato4;
	}

	public String getDato5() {
		return dato5;
	}

	public void setDato5(String dato5) {
		this.dato5 = dato5;
	}

	public String getDato6() {
		return dato6;
	}

	public void setDato6(String dato6) {
		this.dato6 = dato6;
	}

	public String getDato7() {
		return dato7;
	}

	public void setDato7(String dato7) {
		this.dato7 = dato7;
	}

	public String getDato8() {
		return dato8;
	}

	public void setDato8(String dato8) {
		this.dato8 = dato8;
	}

	public String getDato9() {
		return dato9;
	}

	public void setDato9(String dato9) {
		this.dato9 = dato9;
	}

	public String getDato10() {
		return dato10;
	}

	public void setDato10(String dato10) {
		this.dato10 = dato10;
	}

	public ArrayList<Reportes> getReporte() {
		return reporte;
	}

	public void setReporte(ArrayList<Reportes> reporte) {
		this.reporte = reporte;
	}

	private String dato1;
	private String dato2;
	private String dato3;
	private String dato4;
	private String dato5;
	private String dato6;
	private String dato7;
	private String dato8;
	private String dato9;
	private String dato10;
	private ArrayList<Reportes> reporte;

	
	public Reportes(){
		this.dao= new ReportesDao();
		this.reportes = new ArrayList<String> ();
		reporte = new ArrayList<Reportes>();
	}

	public ReportesDao getDao() {
		return dao;
	}

	public void setDao(ReportesDao dao) {
		this.dao = dao;
	}

	public ArrayList<String> getReportes() {
		return reportes;
	}

	public void setReportes(ArrayList<String> reportes) {
		this.reportes = reportes;
	}
	
	public DefaultTableModel ReadProduccionAnual(){
    	Reportes reportes = new Reportes();
        reportes.dao.ReadProduccionAnual(reportes);
        Object[] col = new Object[]{"produccion"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato()});
    	}
    	return tableModel;
    }
	
	
	public DefaultTableModel ReadPromedioProduccionMensual(){
    	Reportes reportes = new Reportes();
        reportes.dao.ReadPromedioProduccionMensual(reportes);
        Object[] col = new Object[]{"avi_modelo","aviones"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel;
    }
	
	public DefaultTableModel TopTenClientes(){
    	Reportes reportes = new Reportes();
        reportes.dao.TopTenClientes(reportes);
        Object[] col = new Object[]{"pedidos","cli_Nombre"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel;
    }
	
	public DefaultTableModel ModeloAvion(){
    	Reportes reportes = new Reportes();
        reportes.dao.ModeloAvion(reportes);
        Object[] col = new Object[]{"avi_modelo"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato()});
    	}
    	return tableModel;
    }
	
	public DefaultTableModel CantidadMediadeAvionesMes(){
    	Reportes reportes = new Reportes();
        reportes.dao.CantidadMediadeAvionesMes(reportes);
        Object[] col = new Object[]{"avi_modelo","pedidos","mes"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1(),reporte.getDato2()});
    	}
    	return tableModel;
    }
	
	public DefaultTableModel ModeloMasVendido(){
    	Reportes reportes = new Reportes();
        reportes.dao.ModeloMasVendido(reportes);
        Object[] col = new Object[]{"avi_modelo","cantidad"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel TipoAlaUtilizada(){
    	Reportes reportes = new Reportes();
        reportes.dao.TipoAlaUtilizada(reportes);
        Object[] col = new Object[]{"avi_modelo"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel FallosMaterial(){
    	Reportes reportes = new Reportes();
        reportes.dao.FallosMaterial(reportes);
        Object[] col = new Object[]{"fallos"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel FallosPiezas(){
    	Reportes reportes = new Reportes();
        reportes.dao.FallosPiezas(reportes);
        Object[] col = new Object[]{"fallos"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel ListaProvedores(){
    	Reportes reportes = new Reportes();
        reportes.dao.ListaProvedores(reportes);
        Object[] col = new Object[]{"rif","nombre","inicio_operaciones"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1(),reporte.getDato2()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel InventarioMateriales(){
    	Reportes reportes = new Reportes();
        reportes.dao.InventarioMateriales(reportes);
        Object[] col = new Object[]{"nombre","cantidad","mes"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1(),reporte.getDato2()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel InventarioPiezas(){
    	Reportes reportes = new Reportes();
        reportes.dao.InventarioMateriales(reportes);
        Object[] col = new Object[]{"modelo","piezas"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel EspecificacionesModelos(){
    	Reportes reportes = new Reportes();
        reportes.dao.EspecificacionesModelos(reportes);
        Object[] col = new Object[]{"nombre","modelo","altura","capacidad","longitud"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1(),reporte.getDato2(),reporte.getDato3(),reporte.getDato4()});
    	}
    	return tableModel; 
    }
	
	public DefaultTableModel descripcionPiezas(){
    	Reportes reportes = new Reportes();
        reportes.dao.descripcionPiezas(reportes);
        Object[] col = new Object[]{"nombre","Descripcion"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel; 
    }
	public DefaultTableModel ProductoMasPedidoAvion(){
    	Reportes reportes = new Reportes();
        reportes.dao.ProductoMasPedidoAvion(reportes);
        Object[] col = new Object[]{"nombre","pedido"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel; 
    }
	public DefaultTableModel ProductoMasPedidoMaterial(){
    	Reportes reportes = new Reportes();
        reportes.dao.ProductoMasPedidoMaterial(reportes);
        Object[] col = new Object[]{"nombre","pedido"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel; 
    }
	public DefaultTableModel ProductoMasPedidoPieza(){
    	Reportes reportes = new Reportes();
        reportes.dao.ProductoMasPedidoPieza(reportes);
        Object[] col = new Object[]{"nombre","pedido"};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Reportes reporte : reportes.getReporte()) {
    		tableModel.addRow(new Object[]{reporte.getDato(),reporte.getDato1()});
    	}
    	return tableModel; 
    }
	
	
	
}