package com.ucab.airucab.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.Beneficiario;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Empleado;
import com.ucab.airucab.models.Sede;

public class EmpleadoDao extends ConnectionMysql implements Dao<Empleado>  {
	

	public EmpleadoDao( ){
         super();
	}
	

	@Override
	public void create(Empleado empleado) {
		int clave=0; 
		 String	query = "INSERT into EMPLEADO(emp_ci,emp_nombre,emp_fecha_nacimiento,emp_apellido,emp_titulo,emp_cargo_empleado,emp_años_servicio,fk_lug_cod,fk_sed_cod)"+
		"VALUES ('"+empleado.getCi()+"','"+empleado.getNombre()+"','"+empleado.getFecha().getFechaString()+"','"+empleado.getApellido()+"','"+empleado.getTitulo()+"','"+empleado.getCargoempleado()+"','"+empleado.getTiemservicio()+"','"+1+"','"+empleado.getSede()+"')";
		 empleado.setStatus(openClonnectionUpdate(query));
		 clave=empleado.getCi();
		 System.out.println("clave");
		 try { 
			 ResultSet key = this.statement.getGeneratedKeys();
			while(key.next()){
				clave=key.getInt(1);
		   		 System.out.println("clave:"+clave);
		   		 
				}
			if(clave>0) {
		    	for (Beneficiario beneficiario : empleado.getBeneficiarios() ) {
		   		 System.out.println("clave:"+clave);
		    		query = "INSERT into BENEFICIARIO(ben_emp_ci,ben_emp_nombre,ben_emp_apellido,ben_emp_parentesco,fk_emp_ci,fk_sede)"+
		    				"VALUES('"+beneficiario.getCi()+"','"+beneficiario.getNombre()+"','"+beneficiario.getApellido()+"','"+beneficiario.getParentezco()+"','"+clave+"')";
		    		empleado.setStatus(openClonnectionUpdate(query));
			}
		    	}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			closeClonnection();
			}
	}

	@Override
	public void read(Empleado empleado) {
		ResultSet rs = openClonnectionExecute("Select* FROM EMPLEADO WHERE emp_ci= '"+empleado.getCi()+"' ");
		try{
			while (rs.next()) {
				empleado.setCi(rs.getInt("emp_ci"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	@Override
	public void update(Empleado empleado) {
		 String	query = "UPDATE into EMPLEADO(emp_ci,emp_Nombre,emp_fecha_nacimiento,emp_apellido, emp_titulo, emp_cargo_empleado,emp_años_servicio,fk_lug_cod)"+
		"VALUES ('"+empleado.getCi()+"','"+empleado.getNombre()+"','"+empleado.getFecha()+"','"+empleado.getApellido()+"','"+empleado.getDireccion()+"')";
		empleado.setStatus(openClonnectionUpdate(query));
		
	}

	@Override
	public void delete(Empleado empleado) {
		// TODO Auto-generated method stub
		 empleado.setStatus(openClonnectionUpdate("DELETE FROM EMPLEADO WHERE emp_ci= '"+empleado.getCi()+"' "));
		 closeClonnection();
	}

	@Override
	public void report(Empleado empleado) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<Empleado> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM EMPLEADO");
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		
		try{
			while (rs.next()) {
				Empleado empleado = new Empleado();
				empleado.setCi(rs.getInt("emp_ci"));
				empleado.setNombre(rs.getString("emp_nombre"));
				empleado.setApellido(rs.getString("emp_apellido"));

				empleados.add(empleado);
				System.out.println(rs.getInt("emp_ci"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return empleados;
	}
	public ArrayList<Sede>MostrarSedes(){
		ResultSet rs = openClonnectionExecute("Select sed_cod,sed_nombre FROM SEDE ");
		ArrayList<Sede> sedes = new ArrayList<Sede>();
		try{
			while (rs.next()) {
				Sede sede = new Sede();
				sede.setCodigo(rs.getInt("sed_cod"));
				sede.setNombre(rs.getString("sed_nombre"));
				System.out.println(rs.getString("sed_nombre"));
				sedes.add(sede);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}	
			
		return sedes;
		
	}
	public ArrayList<Beneficiario> readAllBeneficiario() {
		ResultSet rs = openClonnectionExecute("Select* FROM BENEFICIARIO");
		ArrayList<Beneficiario> beneficiarios = new ArrayList<Beneficiario>();
		
		try{
			while (rs.next()) {
				Beneficiario beneficiario = new Beneficiario();
				beneficiario.setCi(rs.getInt("ben_emp_ci"));
				beneficiarios.add(beneficiario);
				System.out.println(rs.getInt("ben_emp_ci"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return beneficiarios;
	}
	


}
