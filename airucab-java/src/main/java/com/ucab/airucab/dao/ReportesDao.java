package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Reportes;

public class ReportesDao extends ConnectionMysql  {


	public ReportesDao( ){
         super();
	}

	
	public void ReadProduccionAnual (Reportes reportes){
		int ano = 2011;
		ResultSet rs = openClonnectionExecute("SELECT count(*) as produccion FROM air_ucab.PEDIDO as ped	WHERE  ped.ped_estatus='2' and ped.ped_fecha_pedido between '"+ano+"/01/01' and '"+ano+"/12/31' ");
		try{
			while (rs.next()) {

				Reportes reporte = new Reportes();
				reporte.setDato(rs.getString("produccion"));
				reportes.getReporte().add(reporte);
			
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	public void ReadPromedioProduccionMensual(Reportes reportes){
		int mes = 12;
	
		ResultSet rs = openClonnectionExecute("SELECT avi_modelo, count(*) as aviones FROM air_ucab.PEDIDO, air_ucab.AVION where ped_fecha_pedido between  '2011-12-01' and '2012-01-01' and fk_avi_id = avi_id  group by avi_modelo order by aviones desc");
		try{
			while (rs.next()) {
				Reportes reporte = new Reportes();
				reporte.setDato(rs.getString(1));
				reporte.setDato1(rs.getString(2));
                reportes.getReporte().add(reporte);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	public void TopTenClientes(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT cli_Nombre, count(*) as pedidos FROM air_ucab.PEDIDO as ped , air_ucab.CLIENTE as cli where cli.cli_rif=ped.fk_cli_rif group by cli_Nombre order by pedidos desc limit 10");
		try{
			while (rs.next()) {
				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("pedidos"));
				reporte.setDato1(rs.getString("cli_Nombre"));
                reportes.getReporte().add(reporte);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	public void ModeloAvion(Reportes reportes){
		ResultSet rs = openClonnectionExecute("Select avi_modelo FROM air_ucab.AVION");
		try{
			while (rs.next()) {
				
				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("avi_modelo"));
                reportes.getReporte().add(reporte);

			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	
	public void  CantidadMediadeAvionesMes(Reportes reportes){
		ResultSet rs = openClonnectionExecute(" SELECT avi_modelo , count(*)/30 as pedidos,month(ped_fecha_pedido) as mes FROM air_ucab.AVION, air_ucab.PEDIDO as ped WHERE avi_id=fk_avi_id and ped_estatus=2  group by avi_modelo, mes ");
		try{
			while (rs.next()) {
	
				Reportes reporte = new Reportes();;
				
				reporte.setDato(rs.getString("avi_modelo"));
				reporte.setDato1(rs.getString("pedidos"));
				reporte.setDato2(rs.getString("mes"));
                reportes.getReporte().add(reporte);

			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	public void ModeloMasVendido(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT avi_modelo , count(*) as pedidos FROM air_ucab.AVION, air_ucab.PEDIDO WHERE avi_id=fk_avi_id and ped_estatus=2 group by avi_modelo order by pedidos desc limit 1");
		try{
			while (rs.next()) {

				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("avi_modelo"));
				reporte.setDato1(rs.getString("pedidos"));
                reportes.getReporte().add(reporte);

			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	public void TipoAlaUtilizada(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT  pie.pie_modelo FROM air_ucab.PIEZA_AVION as piea ,air_ucab.PIEZA as pie where pie.pie_nombre='ala' and pie.pie_cod=piea.fk_pie_cod group by pie.pie_modelo limit 1");
		try{
			while (rs.next()) {

				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("pie.pie_modelo"));
                reportes.getReporte().add(reporte);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	public void ProductoMasPedidoAvion(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT avi_modelo, count(*) as avion_pedido  FROM air_ucab.PEDIDO, air_ucab.AVION where fk_avi_id = avi_id group by avi_modelo order by avion_pedido desc limit 1");	
		try{
			while (rs.next()) {
				Reportes reporte = new Reportes();
				reporte.setDato(rs.getString("avi_modelo"));
				reporte.setDato1(rs.getString("avion_pedido"));
				reportes.getReporte().add(reporte);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	public void ProductoMasPedidoPieza(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT pie_nombre, count(*) as pieza_pedido  FROM air_ucab.PIEZA, air_ucab.PIEZA_PEDIDO where fk_pie_cod = pie_cod group by pie_nombre order by pieza_pedido desc limit 1");
		try{
			while (rs.next()) {
				Reportes reporte = new Reportes();
				reporte.setDato(rs.getString("pie_nombre"));
				reporte.setDato1(rs.getString("pieza_pedido"));
				reportes.getReporte().add(reporte);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	public void ProductoMasPedidoMaterial(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT mat_nombre, count(*) as proveedor_pedido  FROM air_ucab.PROVEEDOR_PEDIDO, air_ucab.MATERIAL where fk_mat_cod = mat_cod group by mat_nombre order by proveedor_pedido desc limit 1");
		try{
			while (rs.next()) {
				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("mat_nombre"));
				reporte.setDato1(rs.getString("proveedor_pedido"));
				reportes.getReporte().add(reporte);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	public void FallosMaterial(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT  count(*) as fallos FROM air_ucab.MATERIAL as mat , air_ucab.MATERIAL_EVALUACION as mate where mate.pie_eva_status=2 and  mate.fk_mat_cod=mat.mat_cod");
		try{
			while (rs.next()) {
				
				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("fallos"));
                reportes.getReporte().add(reporte);

			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	public void FallosPiezas(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT  count(*) as fallos FROM  air_ucab.PIEZA_PRUEBA as piepr ,air_ucab.PIEZA_PEDIDO as piep where piepr.pie_Pru_status=2 and piep.pie_ped_id =piepr.fk_pie_ped_id ");
		try{
			while (rs.next()) {
				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("fallos"));
                reportes.getReporte().add(reporte);

			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	public void ListaProvedores(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT * FROM air_ucab.PROVEEDOR");
		try{
			while (rs.next()) {
				
				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("pro_rif"));
				reporte.setDato1(rs.getString("pro_nombre"));
				reporte.setDato2(rs.getString("pro_fecha_inicio_operaciones"));
                reportes.getReporte().add(reporte);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	public void InventarioMateriales(Reportes reportes){
		ResultSet rs = openClonnectionExecute("select a.mat_nombre,sum(a.pro_ped_cantidad) as cantidad,a.mes from (SELECT mat_nombre, pro_ped_cantidad , month(pro_ped_fecha_inicio) as mes FROM air_ucab.PROVEEDOR_PEDIDO as ped, air_ucab.MATERIAL as mat where fk_mat_cod = mat_cod and pro_ped_fecha_inicio between '2011-12-01' and '2011-12-31')  as a   group by a.mat_nombre,a.mes ");
		try{
			while (rs.next()) {

				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("a.mat_nombre"));
				reporte.setDato1(rs.getString("cantidad"));
				reporte.setDato2(rs.getString("a.mes"));
                reportes.getReporte().add(reporte);	
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	public void InventarioPiezas(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT pie_modelo, count(*) as piezas FROM air_ucab.PIEZA_PEDIDO, air_ucab.PIEZA where fk_pie_cod = pie_cod  group by pie_modelo order by piezas desc");
		try{
			while (rs.next()) {
				
				Reportes reporte = new Reportes();;
				reporte.setDato(rs.getString("pie_modelo"));
				reporte.setDato1(rs.getString("piezas"));
                reportes.getReporte().add(reporte);
		
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	public void EspecificacionesModelos(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT * FROM air_ucab.AVION");
		try{
			while (rs.next()) {

				
				Reportes reporte = new Reportes();
				reporte.setDato(rs.getString("avi_nombre"));
				reporte.setDato1(rs.getString("avi_modelo"));
				reporte.setDato2(rs.getString("avi_altura"));
				reporte.setDato3(rs.getString("avi_capacidad"));
				reporte.setDato4(rs.getString("avi_longitud"));
                reportes.getReporte().add(reporte);
		
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
		
	}
	public void descripcionPiezas(Reportes reportes){
		ResultSet rs = openClonnectionExecute("SELECT * FROM air_ucab.PIEZA");
		try{
			while (rs.next()) {
				Reportes reporte = new Reportes();

				reporte.setDato(rs.getString("pie_nombre"));
				reporte.setDato1(rs.getString("pie_descripcion"));
				reportes.getReporte().add(reporte);
				
		
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			closeClonnection();
		}
	}
	
	

}

	