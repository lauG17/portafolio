package com.ucab.airucab.dao;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Factura;
import com.ucab.airucab.models.Material;

public class FacturaDao extends ConnectionMysql implements Dao<Factura> {
	
	public FacturaDao(){
		super();
	}
	


	@Override
	public void create(Factura factura) {
		    String query= "INSERT into FACTURA(fac_monto_pagar,fac_fecha_emision,fk_fac_tip_codigo)"+
				"VALUES('"+ factura.GetPago() +"','"+ factura.getFechaString() +"','"+ factura.getTipoPago() +"')";
		    System.out.println(query);
		    factura.setStatus(openClonnectionUpdate(query));
		 	int clave =0; 
			try { 
				ResultSet key = this.statement.getGeneratedKeys();
				while(key.next()){
					clave=key.getInt(1);
					System.out.println("clave"+clave);
					}
				if(clave>0) 
					factura.setId(clave);
				}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				closeClonnection();
			}
			
			
		
	}

	@Override
	public void read(Factura factura) {
		ResultSet rs = openClonnectionExecute("Select* FROM FACTURA WHERE fac_codigo = '"+factura.getId()+"' ");
		try{
			while (rs.next()) {
				factura.setId(rs.getInt("fac_codigo"));
				factura.setPago(rs.getInt("fac_monto_pagar"));
				factura.setFechaString(rs.getString("fac_fecha_emision"));
				factura.setTipoPago(rs.getInt("fk_fac_tip_codigo"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}

	@Override
	public void delete(Factura factura) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void report(Factura factura) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void update(Factura factura) {
		// TODO Auto-generated method stub
		
	}

	

}
