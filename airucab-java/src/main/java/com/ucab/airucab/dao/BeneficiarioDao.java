package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Empleado;
import com.ucab.airucab.models.Beneficiario;

public class BeneficiarioDao extends ConnectionMysql implements Dao<Beneficiario> {
	
	public BeneficiarioDao(){
		super();
	}
	
	@Override
	public void create(Beneficiario beneficiario) {
		String query= "INSERT into BENEFICIARIO(ben_emp_ci,ben_emp_nombre,ben_emp_apellido,ben_emp_parentesco,fk_empleado)"+
				"VALUES('"+beneficiario.getCi()+"','"+beneficiario.getNombre()+"','"+beneficiario.getApellido()+"','"+beneficiario.getParentezco()+"')";
		 beneficiario.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
		
	}

	@Override
	public void read(Beneficiario beneficiario) {
		ResultSet rs = openClonnectionExecute("Select* FROM BENEFICIARIO WHERE ben_emp_ci= '"+beneficiario.getCi()+"' ");
		try{
			while (rs.next()) {
				beneficiario.setCi(rs.getInt("ben_emp_ci"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

		@Override
	public void update(Beneficiario beneficiario) {
		String	query = "UPDATE into BENEFICIARIO(ben_emp_ci,ben_emp_Nombre,ben_emp_apellido,ben_emp_parentesco)"+
		"VALUES ('"+beneficiario.getCi()+"','"+beneficiario.getNombre()+"','"+beneficiario.getApellido()+"','"+beneficiario.getParentezco()+"')";
		beneficiario.setStatus(openClonnectionUpdate(query));
		
	}

	@Override
	public void delete(Beneficiario beneficiario) {
		beneficiario.setStatus(openClonnectionUpdate("DELETE FROM BENEFICIARIO WHERE emp_ci= '"+beneficiario.getCi()+"' "));
		 closeClonnection();
		
	}

	@Override
	public void report(Beneficiario beneficiario) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Beneficiario> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM BENEFICIARIO");
		ArrayList<Beneficiario> beneficiarios = new ArrayList<Beneficiario>();
		
		try{
			while (rs.next()) {
				Beneficiario beneficiario = new Beneficiario();
				beneficiario.setCi(rs.getInt("ben_emp_ci"));
				beneficiarios.add(beneficiario);
				System.out.println(rs.getInt("ben_emp_ci"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return beneficiarios;
	}
	

}
