package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Zona;

public class ZonaDao extends ConnectionMysql implements Dao<Zona> {

	public ZonaDao()  {
		super();
	}

	@Override
	public void create(Zona zona) {
		 String	query = "INSERT into ZONA(zo_nombre)"+
					"VALUES ('"+zona.getNombre()+"')";
		System.out.println(query);			 
		 zona.setStatus(openClonnectionUpdate(query));
					 closeClonnection();
		
	}

	@Override
	public void read(Zona zona) {
		ResultSet rs = openClonnectionExecute("Select* FROM ZONA WHERE zo_cod= '"+zona.getId()+"' ");
		try{
			while (rs.next()) {
				zona.setId(rs.getInt("zo_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}

	public void readAll(Zona zona) {
		ResultSet rs = openClonnectionExecute("Select* FROM ZONA ");
		try{
			while (rs.next()) {
				zona.setId(rs.getInt("zo_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}
	
	@Override
	public void update(Zona zona) {
	  String	query = "UPDATE into ZONA(zo_Nombre)"+
	  "VALUES ('"+zona.getNombre()+"')";
	  zona.setStatus(openClonnectionUpdate(query));
				
	} 

	@Override
	public void delete(Zona zona) {
		 String	query = "DELETE FROM ZONA WHERE zo_cod = "+zona.getId();			 
		 zona.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
		
	}

	@Override
	public void report(Zona zona) {
		// TODO Auto-generated method stub
		
	}

}
