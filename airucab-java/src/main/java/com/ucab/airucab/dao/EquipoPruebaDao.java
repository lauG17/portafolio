package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Empleado;
import com.ucab.airucab.models.EquipoPrueba;
import com.ucab.airucab.models.Material;
import com.ucab.airucab.models.Sede;

public class EquipoPruebaDao extends ConnectionMysql implements Dao<EquipoPrueba> {

	public EquipoPruebaDao(){
		super();
	}
	

	
	public ArrayList<Empleado> mostrarEmpleados() {
		ResultSet rs = openClonnectionExecute("Select emp_ci,emp_nombre FROM EMPLEADO, SEDE WHERE fk_sed_cod=sed_cod");
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		
		try{
			while (rs.next()) {
				Empleado empleado = new Empleado();
				empleado.setCi(rs.getInt("emp_ci"));
				empleado.setNombre(rs.getString("emp_nombre"));
				System.out.println(rs.getString("emp_nombre"));
				empleados.add(empleado);

				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return empleados;
	}
	
	public ArrayList<Sede>MostrarSedes(){
		ResultSet rs = openClonnectionExecute("Select sed_cod,sed_nombre FROM SEDE ");
		ArrayList<Sede> sedes = new ArrayList<Sede>();
		try{
			while (rs.next()) {
				Sede sede = new Sede();
				sede.setCodigo(rs.getInt("sed_cod"));
				sede.setNombre(rs.getString("sed_nombre"));
				System.out.println(rs.getString("sed_nombre"));
				sedes.add(sede);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}	
			
		return sedes;
		
	}
	
	
	@Override
	public void create(EquipoPrueba dao) {
		 String	query = "INSERT into EQUIPO_PRUEBA_PIEZA(equ_pru_pie_cod,equ_pru_pie_nombre,equ_pru_pie_fecha_inicio,equ_pru_pie_fecha_fin)"+
		"VALUES ('"+dao.getCodigo()+"','"+dao.getNombre()+"','"+dao.getFechainicio().getFechaString()+"','"+dao.getFechafin().getFechaString()+"')";
		 dao.setStatus(openClonnectionUpdate(query));		
	}
	@Override
	public void read(EquipoPrueba dao) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update(EquipoPrueba dao) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void delete(EquipoPrueba dao) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void report(EquipoPrueba dao) {
		// TODO Auto-generated method stub
		
	}

}
