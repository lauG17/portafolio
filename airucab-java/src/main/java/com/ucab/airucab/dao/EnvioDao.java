package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Envio;

public class EnvioDao  extends ConnectionMysql implements Dao<Envio> {


	public EnvioDao( ){
         super();
	}
	

	@Override
	public void create(Envio envio) {
		 String d = "11/12/12";
		 String e = "12/12/12";
		 String	query = "INSERT into ENVIO(env_estatus_envio,env_fecha_inicio,env_fecha_fin)"+
		"VALUES ('"+envio.getStatusEnvio()+"','"+d+"','"+e+"','"+1+"')";
		 envio.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	@Override
	public void read (Envio envio) {
		ResultSet rs = openClonnectionExecute("Select* FROM ENVIO WHERE env_codigo= '"+envio.getCodigo()+"' ");
		try{
			while (rs.next()) {
				envio.setCodigo(rs.getInt("env_codigo"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	@Override
	public void update(Envio envio) {
		 String	query = "UPDATE into ENVIO(env_codigo,env_estatus_envio,env_fecha_inicio,env_fecha_fin)"+
		 "VALUES ('"+envio.getCodigo() +"','"+envio.getStatusEnvio() +"','"+envio.getMontoAcreditado()+"','"+envio.getFecha()+"','"+envio.getFecha()+"')";
		 envio.setStatus(openClonnectionUpdate(query));
		
		
	}

	@Override
	public void delete(Envio envio) {
		 envio.setStatus(openClonnectionUpdate("DELETE FROM ENVIO WHERE env_codigo= '"+envio.getCodigo()+"' "));
		 closeClonnection();
	}		
	

	@Override
	public void report(Envio envio) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<Envio> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM ENVIO");
		ArrayList<Envio> envios = new ArrayList<Envio>();
		
		try{
			while (rs.next()) {
				Envio envio = new Envio();
				envio.setCodigo(rs.getInt("env_codigo"));
				envios.add(envio);
				System.out.println(rs.getString("env_codigo"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return envios;
	}
	




}
