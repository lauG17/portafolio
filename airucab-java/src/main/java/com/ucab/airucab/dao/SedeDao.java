package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Sede;

public class SedeDao extends ConnectionMysql implements Dao<Sede> {

	
	public SedeDao( ){
        super();
	}
	@Override
	public void create(Sede sede) {
		// TODO Auto-generated method stub
		 String	query = "INSERT into SEDE(sed_cod,sed_nombre,sed_tipo,fk_lug_cod)"+
				 "VALUES ('"+sede.getCodigo()+"','"+sede.getNombre()+"','"+sede.getTipo()+"','"+1+"')";
		 sede.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	@Override
	public void read(Sede sede) {
		ResultSet rs = openClonnectionExecute("Select* FROM SEDE WHERE sed_cod= '"+sede.getCodigo()+"' ");
		try{
			while (rs.next()) {
				sede.setCodigo(Integer.parseInt(rs.getString("sed_cod")));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}		
	}

	@Override
	public void update(Sede sede) {
		 String	query = "UPDATE into SEDE(sed_cod,sed_nombre,sed_tipo,fk_lug_cod)"+
			"VALUES ('"+sede.getCodigo()+"','"+sede.getNombre()+"','"+sede.getTipo()+"','"+1+"')";
		 	sede.setStatus(openClonnectionUpdate(query));
							
	}

	@Override
	public void delete(Sede sede) {
		sede.setStatus(openClonnectionUpdate("DELETE FROM SEDE WHERE sed_cod= '"+sede.getCodigo()+"' "));
		closeClonnection();
	}

	@Override
	public void report(Sede sede) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Sede> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM SEDE");
		ArrayList<Sede> sedes = new ArrayList<Sede>();
		
		try{
			while (rs.next()) {
				Sede sede = new Sede();
				sede.setCodigo(rs.getInt("sed_cod"));
				sedes.add(sede);
				System.out.println(rs.getInt("sed_cod"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return sedes;
	}

}
