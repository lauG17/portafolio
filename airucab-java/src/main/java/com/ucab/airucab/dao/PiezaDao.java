package com.ucab.airucab.dao;

import java.sql.ResultSet;	
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.*;

public class PiezaDao extends ConnectionMysql implements Dao<Pieza> {

	public PiezaDao()  {
		// TODO Auto-generated constructor stub
	}
	
	
	public void pedidoPieza(Pieza pieza,int pedido ) {
		String	query = "INSERT into PIEZA_PEDIDO(pie_ped_fecha_inicio,pie_ped_fecha_fin,pie_ped_status,fk_pie_cod,fk_pie_pedido)"+
		"VALUES ('"+"11/12/12"+"','"+"11/12/12"+"','"+1+"','"+pieza.getId()+"','"+pedido+"')";
		System.out.println(query);
		openClonnectionUpdate(query);
		closeClonnection();
	}

	@Override
	public void create(Pieza pieza) {
		int clave =0; 
		String	query = "INSERT into PIEZA(pie_Nombre,pie_descripcion,pie_duracion_construccion,pie_modelo)"+
		"VALUES ('"+pieza.getNombre()+"','"+pieza.getDescripcion() +"','"+pieza.getDuraccion()+"','"+pieza.getModelo() +"')";
		System.out.println("daoPieza"); 
		//dao.setStatus(openClonnectionUpdate(query));
		openClonnectionUpdate(query);
		try { 
			ResultSet key = this.statement.getGeneratedKeys();
			while(key.next()){
				clave=key.getInt(1);
				System.out.println("clave"+clave);
				}
			if(clave>0) {
		    	for (Material material : pieza.getMateriales() ) {
		    		query = "INSERT into PIEZA_MATERIAL(pie_mat_cantidad,fk_pie_cod,fk_mat_cod)"+
		    				"VALUES ('"+material.getCantidad()+"','"+clave+"','"+material.getId()+"')";
		    		pieza.setStatus(openClonnectionUpdate(query));
		  	    }}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			closeClonnection();
			}
		
	}

	@Override
	public void read(Pieza dao) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Pieza dao) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Pieza dao) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void report(Pieza dao) {
		// TODO Auto-generated method stub
		
	}
	
	
	public ArrayList<Material> readAll() { 
		ResultSet rs = openClonnectionExecute("Select* FROM MATERIAL");
		ArrayList<Material> materiales = new ArrayList<Material>();
		
		try{
			while (rs.next()) {
				Material material = new Material();
				material.setId(rs.getInt("mat_cod"));
				material.setNombre(rs.getString("mat_nombre"));
				System.out.println(rs.getString("mat_nombre"));
				materiales.add(material);
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return materiales;
	}
	
	public void rellenarMateriales(ArrayList<Pieza> piezas) {
		
		for (Pieza pieza : piezas) {
			rellenarMaterial(pieza);
		}   
		
	} 
	
	
	public void rellenarMaterial(Pieza pieza) {
		ResultSet rs = openClonnectionExecute("Select * FROM PIEZA_MATERIAL where fk_pie_cod = "+pieza.getId());

		System.out.println("Select * FROM PIEZA_MATERIAL where fk_pie_cod = "+pieza.getId());
		try{
			while (rs.next()) {
				Material material = new Material();
				material.setId(rs.getInt("fk_mat_cod"));
				System.out.println( material.getId() );
				material.setCantidad(rs.getInt("pie_mat_cantidad"));
				pieza.getMateriales().add(material);
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	} 
	
	public ArrayList<Pieza> readAllPieza() {
		ResultSet rs = openClonnectionExecute("Select* FROM PIEZA");
		ArrayList<Pieza> piezas = new ArrayList<Pieza>();		
		try{
			while (rs.next()) {
				Pieza pieza = new Pieza();
				pieza.setId(rs.getInt("pie_cod"));
				pieza.setNombre(rs.getString("pie_nombre"));
				pieza.setDescripcion(rs.getString("pie_descripcion"));
				pieza.setDuraccion(rs.getInt("pie_duracion_construccion"));
				System.out.println(rs.getString("pie_nombre"));
				piezas.add(pieza);				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return piezas;
	}
	
	public ArrayList<Prueba> readAllPrueba() {
		ResultSet rs = openClonnectionExecute("Select* FROM PRUEBA");
		ArrayList<Prueba> pruebas = new ArrayList<Prueba>();
		
		try{
			while (rs.next()) {
				Prueba prueba = new Prueba();
				prueba.setId(rs.getInt("pru_cod"));
				prueba.setNombre(rs.getString("pru_nombre"));
				prueba.setTiempo(rs.getInt("pru_duracion"));
				System.out.println(rs.getString("pru_nombre"));
				pruebas.add(prueba);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return pruebas;
	}

}
