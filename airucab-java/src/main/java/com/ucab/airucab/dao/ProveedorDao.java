package com.ucab.airucab.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Material;
import com.ucab.airucab.models.Proveedor;

public class ProveedorDao extends ConnectionMysql implements Dao<Proveedor>  {
	
	
	
	
	public ProveedorDao( ){
         super();
	}
	
	public void pedidoProveedor( Proveedor proveedor ) {
		 String	query = "INSERT into PROVEEDOR_PEDIDO(pro_ped_fecha_inicio,pro_ped_Fecha_fin,"
		 		+ "pro_ped_status,pro_ped_cantidad,fk_pro_rif,fk_mat_cod,fk_sed_cod)"+
		"VALUES ('18/12/12','"+"18/12/12" +"','"+1+"','"+proveedor.getMaterial().getCantidad()+"','"+proveedor.getRif() +"','"+proveedor.getMaterial().getId()+"','"+1+"')";
		 System.out.println(query);
		 proveedor.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}
	
	
	
	public Proveedor proveedorMaterial(Material material) {
		ResultSet rs = openClonnectionExecute("Select * FROM PROVEEDOR where fk_material = "+ material.getId() );
		System.out.println("material "+ material.getId());
		Proveedor proveedor = new Proveedor();
		
		try{ 

			while (rs.next()) {
			    proveedor = new Proveedor();
				proveedor.setRif(rs.getString("pro_rif"));
				proveedor.setNombre(rs.getString("pro_Nombre"));
				proveedor.getFecha().setFechaString(rs.getString("pro_fecha_inicio_operaciones"));
				proveedor.getMaterial().setId(material.getId());
                System.out.println(rs.getString("pro_rif")+"material");
                break;
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return proveedor;
	}

	@Override
	public void create(Proveedor proveedor) {
		 String	query = "INSERT into PROVEEDOR(pro_rif,pro_Nombre,pro_fecha_inicio_operaciones,fk_lug_cod)"+
		"VALUES ('"+proveedor.getRif()+"','"+proveedor.getNombre()+"','"+proveedor.getFecha().getFechaString()+"','"+1+"')";
		 proveedor.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	@Override
	public void read(Proveedor proveedor) {
		ResultSet rs = openClonnectionExecute("Select* FROM PROVEEDOR WHERE pro_rif= '"+proveedor.getRif()+"' ");
		try{
			while (rs.next()) {
				proveedor.setRif(rs.getString("pro_rif"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	@Override
	public void update(Proveedor proveedor) {
		 String	query = "UPDATE into PROVEEDOR(pro_rif,pro_Nombre,pro_fecha_inicio_operaciones,fk_lug_cod)"+
		"VALUES ('"+proveedor.getRif()+"','"+proveedor.getNombre()+"','"+proveedor.getFecha()+"','"+proveedor.getDireccion()+"')";
		proveedor.setStatus(openClonnectionUpdate(query));
		
		
	}

	@Override
	public void delete(Proveedor proveedor) {
		 proveedor.setStatus(openClonnectionUpdate("DELETE FROM PROVEEDOR WHERE pro_rif= '"+proveedor.getRif()+"' "));
		 closeClonnection();
	}

	@Override
	public void report(Proveedor proveedor) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Proveedor> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM PROVEEDOR");
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
		
		try{
			while (rs.next()) {
				Proveedor proveedor = new Proveedor();
				proveedor.setRif(rs.getString("pro_rif"));
				proveedor.setNombre(rs.getString("pro_Nombre"));
				proveedor.getFecha().setFechaString(rs.getString("pro_fecha_inicio_operaciones"));
				proveedores.add(proveedor);
				System.out.println(rs.getString("pro_rif"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return proveedores;
	}

}
